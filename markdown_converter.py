import markdown
import webapp.layouts

from markdown.treeprocessors import Treeprocessor
from markdown.extensions import Extension


class AddClassesToTables(Treeprocessor):
    def run(self, root):
        tables = root.findall('table')
        for table in tables:
            table.attrib['class'] = 'simple'
            table.attrib['cellspacing'] = '0'


class AddClassesToTableExtension(Extension):
    def extendMarkdown(self, md):
        # Register instance of 'mypattern' with a priority of 175
        md.treeprocessors.register(AddClassesToTables(), 'addclasstotable', 100)


if __name__ == "__main__":
    with open("readme.md", 'r') as f:
        markdown_source = f.read()

    html = markdown.markdown(markdown_source, extensions=['tables', AddClassesToTableExtension()])

    with open('webapp/static/doc/about.html', 'w') as f:
        f.write(webapp.layouts.build_layout(html))
