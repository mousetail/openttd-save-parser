
# About [OpenTTD AI Battle](//openttd.mousetail.nl)

This site attempts to determine which AI's for [OpenTTD](http://openttd.org) perform the best.
There are servers running many matches and based on the results we compute rankings based on the ELO
raking system. Additionally, statistics are collected about every match,
in order to better analyze AI behavior.
Games are played from 1975 to 2025. You can watch the active game by spectating
[openttd.mousetail.nl](openttd://openttd.mousetail.nl:3989).
        
An AI wins by having the most total money (cash - loan + estimated company value) at the end of a specified
        period. Separate rankings
        are calculated for different amounts of years. One AI might perform better after one year, another gets the
        optimal profit after 20.</p>
        
## Statistics Meanings

|Value    | Meaning     |
|---------|-------------| 
|**Money**    |This is used for raking the AI's. It is computed based on cash - loan + estimated company value |
|**Loan**     |The loan of the company |
|**Cash**    |The amount the company directly has in the bank.|
|**Vehicle Value** |The estimated total values of vehicles owned by the company. This value is a rough estimate since some vehicle values are not included in the save file. Usually this value is only significant in very early years.|
|**Total Vehicles**|The total number of vehicles owned by the company. Equals road vehices + trains + ships + planes. In some old matches this value also includes some non-vehicles like airplane shadows or train wagons.|
|**Road Vehicles**|The total number of road vehicles owned by the company.
|**Trains**|The total number of trains owned by the company. Newer matches include only engines, older matches also include wagons.
|**Ships**|The total number of ships owned by the company.
|**Planes**|The total number of planes owned by the company. Older versions include airborn planes twice since the shadow also counts as a vehicle.
|**Map Control**|The total number of tiles "owned" by the company. Level crossings are considered owned by the rail company. Includes many other types of ownership besides road, rail and station.
|**Stations**|The total number of tiles owned by the company where the tile is a station tile.</td>
|**Road Tiles**|The total number of tiles owned by the company where the primary type of the tile is road. Level crossings are considered road tiles but are owned by the company that owns the rail.
|**Rail Tiles**|The total number of tiles owned by the company where the primary type of the tile is rail. Level crossings are not considered rail tiles.
|**Other Tiles**|The number of tiles owned by the company that are not considered road, rail, or station tiles. This includes, for example canals, objects and bridges.

## Game Rules

For the default preset, the game is played on a 256*256 map from 1975
to 2025. The AI with the most total cash wins.

Every game is run in a random "preset", or configuration. Currently, there are 3 presets:

* [Default](//openttd.mousetail.nl/static/configuration/default.ini) has cargodist enabled for PAX, and all other settings default.
* [Ocean](//openttd.mousetail.nl/static/configuration/ocean.ini) has sea level set to max, to promote water-based strategies
* [NewGRF](//openttd.mousetail.nl/static/configuration/newgrf.ini) has the following newGRFs enabled:
    * OpenGFX Road Vehicles 0.4.1
    * OpenGFX Trees 0.8.0
    * OpenGFX Landscape 0.2.3
    * OpenGFX Trains 0.3.0
    * FIRS Industry Replacement Set 3.0.3
    * FISH 2.0.3
    * Total Town Replacement Set 3.14
    * AV8 Aviators Aircraft Set 2.21

Shorter games allow me to run more games in less time, so the rankings will stabilize faster.

### Metrics

A game is scored on multiple metrics. A ranking is generated for every metric. Currently, the following metrics exist:

* *Money* Total cash and vehicle value owned by the company

## Contributing

[Source Link](https://gitlab.com/mousetail/openttd-save-parser)

If you want to help out, here are some ways you can:

* Finding and Reporting Bugs
* Adding more metrics and categories
* Improving the user interface
* Fixing bugs and inconsitencies
* Suggesting more presets
* Running your own test instance (See Below)

## Running the test instance yourself

Since running the game once takes such a long time, it would be good to have more servers running matches.So, if you have an old laptop lying around that is capable of running OpenTTD, consider contributing to the cause. There
is a supervisor script that will handle starting the game with the correct settings and uploading the results. If you
have a server or VPS running with some extra computing power that is also great. Running one game actually requires
very little resources (See the CPU use heading) so you should be able to run many instances at once, and you can adjust the number depending
on what else your server needs to do.

If you want to contribute, you first need an authentication key. Please contact me to get such a key.

### Setting up the supervisor script

The script runs primarily on Linux, while it may be possible to get it to work on windows or mac, this is untested
and you probably will need to work out some issues.

The script requires python 3.5 minimum. First, install the dependencies:

    pip3 install requests
    
I recommend creating a user especially for the game, I call it `openttd1`. If you are running
multiple instances you can call them `openttd2`, `openttd3` etc. Clone the git repository into
the users home folder.

Make sure the user has access to the OpenTTD executable. Make sure the users `.openttd` directory has all AI's installed.
You *cannot* run multiple instances as the same user, as each instance must have it's own `.openttd` directory.
    
Next, create a file `openttd_config.json` under the `gamesupervisor` folder. It should look something like this:

    {
        "openttd_location": "~/openttd/bin",
        "supervisor_url": "http://openttd.mousetail.nl",
        "authentication_key": ""
    }

Where `openttd_location` is the full path to the openttd `bin` folder, and `supervisor_url` is the url of the
server to which the results should be uploaded. I will give you an `authentication_key`.

You will also need to edit the users `openttd.cfg` to remove the `AI` section, since the duplicate keys cause
problems when patching in the preset. You will need to repeat this process if you ever run OpenTTD as the same
user manually, though the script will take care of it itself.

Now, create a blank directory `~/openttd_saves/save`.

To test, run:

    nohup rungame.py &
    
To run more permanently, I suggest starting the game as a service via `systemd`. However, how to do this is outside the
scope of this article.

### Running Multiple Instances

While you could run multiple instances directly with the method above, this is not very efficient with hard drive
space. The `content_download` folder alone can be several gigabytes, and it would be a waste to store
this seperately for every instance.

For this purpose 
The following directories can be shared via symlinks:

 * `~/.openttd/content_download`
 * `openttd-save-parser`
 * the openttd root folder
 
 The following directory must never be shared:
 
  * `~/.openttd/save`
  
 Sharing this directory interferes with the way autosaves work with multiple instances.
 

### CPU Use Estimates

This table estimates how much of your comptuers resources one runner will take, and by extension how many runnens
you can run on. The data is based on peeking at a running processes over some time and taking the maximal value seen
during this time, however the actual values depend on various factors and may be different for different setups.

Do not run as many instances as possible based on one peek at your computers resource use. Otherwise the performance
may degrade when your computer is installing updates or performing a virus scan or any other background task. Keep
a wide margin around your computers maximal specs.

Parameter                 | Value           | 
-------------             |-------------  | 
Processor Use (peak)      | ~10% of 2GHZ   | 
RAM Use                   | ~512MiB|
Disk                      | ~60KiB    |

## AI's With Warnings

These AIs will be banned if they get one more warning

* Orders Assistant AI -- Accused of generating excessive logs
* MedievalAI -- Accused of refusing to start
* Rondje om de Kerk -- Accused of refusing to start
* SynTrans -- Accused of generating so many logs the game lags
* SmallTownAI -- Accused of generating so many logs the game lags
* WMDot -- Accused of going bankrupt

I am not actually sure of it was SynTrans or SmallTownAI that caused the lagging problem,
so I will be running more tests and banning the actual culprit.