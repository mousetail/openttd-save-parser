import subprocess
import json
from webapp import db
import os


def rerun_all_matches():
    database = db.DBHandler()

    with open('config.json', 'r') as f:
        settings = json.load(f)

    with database.get_cursor() as cursor:
        cursor.execute(
            """
            DELETE FROM elos
            WHERE 1=1
            """
        )
        cursor.execute(
            """
            DELETE FROM metrics
            WHERE 1=1
            """
        )

        cursor.connection.commit()

        cursor.execute(
            """
            SELECT
            id,
            save_name,
            preset,
            phase,
            pending_match_id
            FROM matches
            """
        )

        match = cursor.fetchone()
        while match is not None:
            match_id, save_name, preset, phase, pending_match_id = match
            if match_id is None or save_name is None or preset is None or pending_match_id is None:
                print(match_id, save_name, preset, phase, pending_match_id, match_id)

            if pending_match_id is None:
                match = cursor.fetchone()
                continue

            save_filename = os.path.join(settings["saves_location"], save_name.split('/')[-1])

            subprocess.call(
                ["python3", "saveparser/analize.py", save_filename, settings["images_location"], str(pending_match_id),
                 str(preset), str(phase), str(match_id)]
            )

            match = cursor.fetchone()
            print("|", end="")


if __name__ == "__main__":
    #print("Are you sure?")
    #if input("(Y/N").lower() == "y":
    rerun_all_matches()
