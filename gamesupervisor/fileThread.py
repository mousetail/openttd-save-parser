import threading


class NonBlockingFile(object):
    def __init__(self, file):
        self.file = file
        self.buffer = b''
        self.lock = threading.Lock()
        self.thread = threading.Thread(target=self.read_from_file)
        self.thread.start()

    def read_from_file(self):
        while True:
            byte = self.file.read(1)
            if (byte):
                with self.lock:
                    self.buffer += byte
            else:
                return

    def poll(self):
        return b'\n' in self.buffer

    def readline(self) -> bytes:
        if self.poll():
            with self.lock:
                index = self.buffer.index(b'\n')
                line = self.buffer[:index]
                self.buffer = self.buffer[index + 1:]
                return line
        return b""

    def __del__(self):
        self.thread.join(0.1)
