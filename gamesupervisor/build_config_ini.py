import tempfile
import configparser
import os
import uuid


def build_config(settings):
    if isinstance(settings, str):
        settings_source = settings
        settings = configparser.ConfigParser()
        settings.read_string(settings_source)

    with open(os.path.expanduser('~/.openttd/openttd.cfg'), 'r') as f:
        default_config = configparser.ConfigParser()
        default_config.read_file(f)

    for category, items in settings.items():
        for key, value in items.items():
            default_config[category][key] = str(value)

    tmpdir = os.path.expanduser('~/.openttd')
    filename = os.path.join(tmpdir, str(uuid.uuid4()) + '.cfg')
    with open(filename, 'w') as f:
        default_config.write(f)
    print("Config file location: "+filename)
    return filename


if __name__ == "__main__":
    print(build_config({"network": {"server_port": 77}}))
