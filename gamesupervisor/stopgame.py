import os
import sys
import time


def is_running(pid):
    try:
        os.kill(pid, 0)
    except OSError:
        return False
    return True


if __name__ == "__main__":
    assert len(sys.argv) == 2, str(sys.argv)
    pid = int(sys.argv[1])
    os.kill(pid, 2)

    while is_running(pid):
        time.sleep(0.5)
