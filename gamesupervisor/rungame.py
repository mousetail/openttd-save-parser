import shutil
import subprocess, time, sys, os
import json
import random
import requests
import sentry_sdk

import fileThread
import build_config_ini

S_START = "START"
S_RUNNING = "RUNNING"

LOG_QUOTA_PER_YEAR = 30
DEBUG_LEVELS = ['W', 'I', 'E', 'S', 'P']


def random_password():
    return ''.join((chr(ord('A') + random.randrange(0, 35)) for i in range(16)))


class OpenTTDManager(object):
    server_settings = {"server_advertise": "false",
                       "max_roadveh": 1000,
                       "autosave": 12,
                       "max_companies": 2,
                       "default_company_pass": random_password(),
                       'default_server_name': 'AI Battle (openttd.mousetail.nl)'
                       }

    def __init__(self):
        print("server pw: " + repr(self.server_settings["default_company_pass"]))
        self.ai_failed = False
        self.process = None
        # print self.process
        # print self.process.stdout, ais
        with open(os.path.expanduser('~/openttd_config.json')) as f:
            self.settings = json.load(f)

        self.port = self.settings.get('port', 3979)
        print("port: ", self.port)

        self.save_location = os.path.expanduser('~/openttd_saves')

        result = self.post_request(self.settings["supervisor_url"] + '/api/start_match')

        result.raise_for_status()

        self.match_info = result.json()
        self.ais = [i["name"] for i in (self.match_info["ai1"], self.match_info["ai2"])]
        self.server_settings = self.__class__.server_settings
        self.server_settings['server_name'] = "\"" + self.server_settings["default_server_name"] + " " + " vs. ".join(
            self.ais) + "\""

        self.match_id = self.match_info["id"]

        self.log_quota = {}
        self.reset_log_quota()

        self.state = S_START
        self.buffer = b""
        self.numIterations = 0
        self.charsReceived = 0
        self.commandIndex = 0
        self.commandQue = []
        self.lastCommand = time.perf_counter()
        self.saveStr = "_vs_".join(self.ais) + time.strftime("_%Y_%m_%d_%H_%M_%S")
        os.mkdir(os.path.join(self.save_location, "save", self.saveStr))
        self.log = open(os.path.join(self.save_location, "log.log"), "w")
        self.ai_log_names = [os.path.join(self.save_location, "save", self.saveStr, ai + "_log.log") for ai in self.ais]
        self.ailogs = [open(name, "w") for name in self.ai_log_names]
        self.game_year = self.match_info["start_year"]

        preset = self.match_info["preset"]
        response = requests.get(self.settings["supervisor_url"] + '/static/configuration/' + preset + '.ini')
        response.raise_for_status()
        config = build_config_ini.build_config(response.text)

        self.config_file_location = config

    def __enter__(self):
        self.process = subprocess.Popen(
            ['./openttd',
             "-D",
             "-t", str(self.match_info["start_year"]),
             '-d', 'net=9',
             '-c', self.config_file_location
             ],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            cwd=self.settings["openttd_location"])

        self.process_out = fileThread.NonBlockingFile(self.process.stdout)
        self.process_err = fileThread.NonBlockingFile(self.process.stderr)
        print("Process starting up...")

        assert not self.get_game_exited()

        return self

    def post_request(self, endpoint, *args, **kwargs):
        return requests.post(
            endpoint,
            *args,
            headers={
                'x-authentication-key': self.settings['authentication_key']
            },
            **kwargs
        )

    def reset_log_quota(self):
        self.log_quota = {
            number: {
                level: LOG_QUOTA_PER_YEAR
                for level in DEBUG_LEVELS
            }
            for number, ai in enumerate(self.ais)
        }

    def get_game_exited(self):
        return self.process.poll() is not None

    def get_game_finished_normally(self):
        return self.ai_failed or self.game_year >= self.match_info["phases"][-1]

    def update(self):
        self.fix_lines()
        done = self.get_game_exited()
        if (done):
            print("done!")
        self.check_for_autosaves()
        self.numIterations += 1
        if self.process_err.poll():
            new = self.process_err.readline()
            self.process_line(*self.parse(new))
        else:
            time.sleep(0.1)
        if self.process_out.poll():
            line = self.process_out.readline().decode('utf-8',
                                                      errors='backslashreplace')
            if "failed to load the specified ai".lower() in line.lower():
                print("Failed loading AI")
                self.ai_failed = True
                self.process.terminate()
            sys.stdout.write(time.strftime("[OUT: %H:%M:%S]") +
                             line + "\n")
        return not done

    def write_message(self, *message, warning=False):
        sys.stdout.write(time.strftime("[{} %H:%M:%S]").format(self.game_year) + " ".join(message) + '\n')

    def fix_lines(self):
        t = time.perf_counter()
        if len(self.commandQue) > 0 and t > self.lastCommand + 0.1:
            ln = self.commandQue.pop()
            if isinstance(ln, str):
                ln = ln.encode('ASCII')
            self.process.stdin.write(ln)
            self.process.stdin.flush()
            self.lastCommand = t
            self.write_message(">>> " + str(ln))

    def send_line(self, *line):
        ln = " ".join(str(i) for i in line) + "\n"
        t = time.perf_counter()
        if t > self.lastCommand + 0.1:
            self.write_message(">>> " + str(ln.encode("ASCII")))
            self.process.stdin.write(ln.encode('ASCII'))
            self.process.stdin.flush()
            self.lastCommand = t
        else:
            self.commandQue.insert(0, ln.encode('ASCII'))

    def parseSquareBrackets(self, line):
        indexLeftBracket = line.index("[")
        indexRightBracket = line.index("]")
        prefix = line[:indexLeftBracket].strip()
        category = line[indexLeftBracket + 1:indexRightBracket]
        content = line[indexRightBracket + 1:].strip()
        return prefix, category, content

    def parse(self, line) -> (str, str, str):
        try:
            prefix, date, content = self.parseSquareBrackets(line.decode('utf-8'))
            prefix2, category, content = self.parseSquareBrackets(content)
        except ValueError as ex:  # no brackets found
            # sys.stderr.write(str(ex))
            prefix = ""
            category = ""
            content = line.decode('utf-8').strip()
        return prefix, category, content

    def process_line(self, prefix, category, content):
        if self.state == S_START:
            if "starting game" in content:
                self.send_line("echo", "starting game")
                for name, value in self.server_settings.items():
                    self.send_line("setting", name, value)

                self.send_line('list_ai')

                for ai in self.ais:
                    self.send_line("start_ai", '"' + ai + '"')

                self.send_line("developer", 1)
                self.send_line("debug_level", "script=4")

                self.send_line("content update")
                self.send_line("content upgrade")
                self.send_line("content download")

                self.state = S_RUNNING
                self.command_index = 0
            else:
                self.write_message(content)
                # print "["+prefix+"]"
        elif self.state == S_RUNNING:
            if category == "script" and '[' in content and ']' in content:
                pre, script_id, content = self.parseSquareBrackets(content)
                pre, debug_level, content = self.parseSquareBrackets(content)
                debug_level = debug_level.upper()
                script_id = int(script_id)
                if self.log_quota[script_id][debug_level] >= 0:
                    self.log_quota[script_id][debug_level] -= 1
                    self.ailogs[script_id].write(time.strftime("[%H:%M:%S]") + content + "\n")
                    self.ailogs[script_id].flush()
            elif "failed to load the specified ai".lower() in content.lower():
                print("Failed loading AI")
                self.ai_failed = True
                self.process.terminate()
            else:
                self.write_message('[cat: {}]'.format(category) + content)
                print(time.strftime("[%H:%M:%S]"), content, file=self.log)

    def check_for_autosaves(self):
        # One game day takes 2220ms (2.22 seconds) real time.
        # A game year thus takes 810.3 seconds, or 13.505 minutes
        # This function should thus run around every 13 minutes
        # GL
        s_dir = os.listdir(os.path.expanduser("~/.openttd/save/autosave"))
        if len(s_dir) > 0:
            f = s_dir[0]
            shutil.move(
                os.path.join(os.path.expanduser("~/.openttd/save/autosave"), f),
                os.path.join(self.save_location, "save", self.saveStr, str(self.game_year) + ".sav")
            )
            self.game_year += 1

            if self.game_year in self.match_info["phases"]:
                save_path = os.path.join(self.save_location, "save", self.saveStr, str(self.game_year - 1) + '.sav')

                save_size = os.stat(save_path).st_size
                while os.stat(save_path).st_size < 4000 or os.stat(save_path).st_size != save_size:
                    save_size = os.stat(save_path).st_size
                    time.sleep(0.4)
                time.sleep(0.4)
                self.write_message("Autosave ready, uploading phase {}".format(self.game_year - 1))
                with open(os.path.join(self.save_location, "save", self.saveStr, str(self.game_year - 1) + '.sav'),
                          'rb') as f:
                    data = f.read()
                    assert data[:3] == b'OTT'
                    response = self.post_request(self.settings["supervisor_url"] +
                                             '/api/submit_results/id/' + str(self.match_id) +
                                             '/phase/' + str(self.game_year) +
                                             '/preset/' + str(self.match_info["preset"]),
                                             data=data)
                    response.raise_for_status()

                for i, log_name in enumerate(self.ai_log_names):
                    with open(log_name, 'rb') as f:
                        response = self.post_request(self.settings["supervisor_url"] +
                                                 '/api/submit_logs/id/{id}/phase/{phase}'
                                                 '/preset/{preset}/ai/{ai}'.format(
                                                     id=self.match_id,
                                                     phase=self.game_year,
                                                     preset=self.match_info["preset"],
                                                     ai=i + 1
                                                 ), data=f)
                        response.raise_for_status()

                self.write_message("Upload Complete, resuming game...")

                if self.get_game_finished_normally():
                    print("Last Phase Reached, exiting game...")
                    self.process.terminate()
            self.reset_log_quota()

            if self.game_year == self.match_info["start_year"] + 2:
                self.send_line("content upgrade")
            elif self.game_year == self.match_info["start_year"] + 3:
                self.send_line("content download")

    def __exit__(self, exc_t, exc_v, trace):
        if not self.get_game_exited():
            self.process.terminate()
        self.log.close()
        for i in self.ailogs:
            i.close()
        print("num iterations: " + str(self.numIterations))
        print("chars: " + str(self.charsReceived))
        print("Exit code: " + str(self.process.wait()))


if __name__ == "__main__":
    with open(os.path.expanduser('~/openttd_config.json')) as f:
        config = json.load(f)
    if 'sentry_url' in config:
        sentry_sdk.init(config["sentry_url"])

    while True:
        with OpenTTDManager() as man:
            while man.update() and not man.get_game_finished_normally():
                pass
            if not man.get_game_finished_normally():
                break
