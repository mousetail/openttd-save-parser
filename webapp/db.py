import json
import os

import psycopg2
import importlib


class DBHandler(object):
    def __init__(self):
        with open('config.json') as f:
            self.config = json.load(f)['db']

        self.db = psycopg2.connect(host=self.config["host"],
                                   user=self.config["user"],
                                   dbname=self.config["user"],
                                   password=self.config["password"])

    def get_cursor(self) -> psycopg2._psycopg.cursor:
        try:
            return self.db.cursor()
        except psycopg2.InterfaceError:
            self.db = psycopg2.connect(host=self.config["host"],
                                       user=self.config["user"],
                                       dbname=self.config["user"],
                                       password=self.config["password"])
            return self.db.cursor()

    def create_initial_table_structure(self, clear_all=False):
        cursor = self.get_cursor()

        if clear_all:
            cursor.execute("DROP TABLE IF EXISTS matches")
            cursor.execute("DROP TABLE IF EXISTS pending_matches")
            cursor.execute("DROP TABLE IF EXISTS elo")
            cursor.execute("DROP TABLE IF EXISTS ai")

        self.db.commit()
        cursor.close()

    def migrate(self):
        print("Starting Migration")
        cursor = self.get_cursor()
        cursor.execute("""
        CREATE TABLE IF NOT EXISTS migrations (
            name VARCHAR PRIMARY KEY,
            applied BOOLEAN,
            applied_date DATE DEFAULT(now())
        )
        """)
        print("Migrations table")

        migrations = os.listdir(os.path.join(os.path.dirname(__file__), "migrations"))
        migrations.sort()

        cursor.execute("""
        SELECT name
        FROM migrations
        WHERE applied=true
        """)
        applied_migrations = tuple(i[0] for i in cursor.fetchall())
        print(applied_migrations)
        for migration in migrations:
            if migration.endswith('.py') and migration != '__init__.py':
                migration = migration[:-3]
                if migration not in applied_migrations:
                    migration_module = importlib.import_module('migrations.' + migration)
                    print(migration, migration_module)
                    migration_module.migrate(cursor)
                    cursor.execute("INSERT INTO migrations(name, applied) VALUES (%s, %s)",
                                   (migration, True))
                    self.db.commit()
        print("Applied all migrations, committing...")
        self.db.commit()
        print("Done!")

    def __del__(self):
        self.db.close()


if __name__ == "__main__":
    DBHandler().migrate()
    if False and input("Are you sure you want to re-create the database?").lower() == "y":
        clear_all = input("Do you want to clear all existing data?").lower() == "y"
        DBHandler().create_initial_table_structure(clear_all=clear_all)
