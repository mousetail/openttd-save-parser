from flask import escape

from .constants import *


def build_layout(sr, title="", canonical=""):
    return """
<!DOCTYPE html>
<html>
    <head>
        <title>{title}OpenTTD AI Rankings</title>
        <meta charset='utf-8'>
        <link rel='stylesheet' href='/static/style.css'>
        <link rel='icon' href='/static/images/logo.png'>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {canonical}
    </head>
    <body>
    <div class="main-content">
        {content}
    </div>

    <div class="footer">
    <p>
        OpenTTD Ranking was built by Maurits van Riezen.</p>
        <p>My other stuff:</p>
        <p>My <a href='https://mousetail.nl'>CV</a></a>
        <p>My money management app: <a href='https://elfsh.mousetail.nl'>
        <img style="width: 1rem" src="https://elfsh.mousetail.nl/static/images/icon_64.png">
        ELFSH</a></p>
        <p>My game about shooting triangles: <a href='https://mousetail.itch.io/lazer-of-death'>
        <img style="width: 1rem" src="/static/images/lod_logo.png">
        Lazer of Death</a></p>
    </div>
    <script async defer src="https://cdn.coollabs.io/save.js"></script>
    </body>
</html>""".format(
        content=sr,
        title=escape(title) + " - " if title else "",
        canonical="<link rel=\"canonical\" href=\"{}\">".format(
            escape(canonical)) if canonical
        else ""
    )


def render_title_with_back(title, escape_title=True):
    if escape_title:
        title = escape(title)
    return '''
    <h1 class='back-icon-container'><a class="back-icon" href="/"></a>{}</h1>
    '''.format(title)


def render_ais(cursor, phase, preset="default"):
    cursor.execute("""
    SELECT ai.id, name, CASE
        WHEN elos.elo is NULL THEN 0
        ELSE elos.elo + 1000
    END
    FROM ai
        LEFT JOIN elos ON ai.id = elos.ai
    WHERE
        (elos.preset = %s OR
            elos.preset IS NULL) AND
        (elos.phase = %s OR
            elos.phase is NULL)
    ORDER BY -elos.elo;
    """, (preset, phase,))
    all_results = cursor.fetchall()

    links_markup = '''
    <p><b>Ranking Categories:</b></p>''' + render_timeline(
        (i, '/ranking/phase/' + str(i) + '/preset/default#ranking') for i in phases
    )

    def format_preset_item(item):
        if item == preset:
            class_name = 'preset-active'
        else:
            class_name = 'preset'
        return '<a href="/ranking/phase/{phase}/preset/{item}#ranking" class={class_name}>{item}</a>'.format(
            phase=escape(phase),
            item=escape(item),
            class_name=escape(class_name)
        )

    presets_markup = '''
    <div class="presets">{}</div>
    '''.format(
        ''.join(format_preset_item(i) for i in display_presets)
    )

    table_markup = '''
    <table cellspacing="0" class="simple">
    <tr>
        <th class="small-column"></th>
        <th scope='col'>Name</th>
        <th scope='col' class="small-column">ELO</th>
    </tr>
    ''' + '\n'.join(

        '<tr><td class="small-column"><div class="rank">#' + str(rank + 1) + '</div></td>' +
        '<td><a href="/ai/{}">{}</a></td><td><a href="/ai/{}">{:,}</a></td></tr>'.format(j[0], j[1], j[0], j[2])
        for rank, j in enumerate(all_results)) + '</table>\n'
    return links_markup + presets_markup + table_markup


def render_timeline(links, left=1970, right=2030):
    return (
        '<div class="timeline">{}</div>').format(
        '\n'.join('<a class="timeline-item" style="left: {0}" href="{2}">'
                  '<div class="timeline-subitem">'
                  '{1}'
                  '</div></a>'.format(
            escape(100 * (i[0] - left) / (right - left)) + '%',
            escape(i[0]),
            escape(i[1])
        )
                  for i in links)
    )


def render_matches(cursor, page, limit=20, mode="page", ai=None,
                   link_format="/matches/page/{}",
                   empty_matches="No matches have been played matching these criteria",
                   filter={}):
    conditions = []
    values = []
    for filterName, filterValue in filter.items():
        if filterName == "phase":
            conditions.append("AND phase=%s ")
            values.append(filterValue)

    cursor.execute("""
    SELECT metrics.elo_1, ai1.name, ai2.name, metrics.elo_2, matches.id, matches.preset, matches.phase
    FROM matches
    INNER JOIN ai AS ai1
        ON ai1.id = matches.opponent_0
    INNER JOIN ai AS ai2
        ON ai2.id = matches.opponent_1
    LEFT JOIN metrics
        ON matches.id= metrics.match
    WHERE 
        (metrics.metric is NULL OR (
            metrics.metric='money' AND
            metrics.preset='overall'
        )) AND
        (%s IS NULL OR matches.opponent_0 = %s OR matches.opponent_1 = %s) {}
    ORDER BY matches.date DESC
    LIMIT (%s)
    OFFSET (%s)
    """.format(
        ''.join(conditions)
    ),
        (ai, ai, ai, *values, limit, page * limit))
    results = cursor.fetchall()

    def render_link(match_id, text):
        return '<a href="/match/' + str(escape(match_id)) + '">' + str(escape(text)) + '</a>'

    def render_single_row(row):
        elo_0, name_0, name_1, elo_1, match_id, preset, year = row
        return """
            <tr>
                <td>{}</td>
                <td class="small-column"><div class="rank">{}</div></td>
                <td class="column-head small-column">VS</td>
                <td>{}</td>
                <td class="small-column"><div class="rank">{}</div></td>
                <td><div class="column-head">{}</div><div>{}</div></td>
            </tr>
        """.format(
            render_link(match_id, name_0),
            render_link(match_id, elo_0 if elo_0 is not None else '--'),
            render_link(match_id, name_1),
            render_link(match_id, elo_1 if elo_1 is not None else '--'),
            render_link(match_id, year),
            render_link(match_id, preset.title()),
        )

    def render_rows(results):
        if len(results) == 0:
            return '<tr><td rowspan="6">{}</td></tr>'.format(escape(empty_matches))
        return ''.join(render_single_row(j) for j in results
                       )

    result = '''
    <table cellspacing="0" class="simple">
        <tr>
            <th scope="col">AI 1 Name</th>
            <th scope="col">Δ ELO</th>
            <th></th>
            <th scope="col">AI 2 Name</th>
            <th scope="col">Δ ELO</th>
            <th scope="col">Year</th>
        </tr>''' + render_rows(results) + '</table>'

    if mode == "page":
        links = ""
        if page != 0:
            links += "<a href='{}'>&lt;&lt;Previous Page</a>".format(
                escape(link_format.format(page - 1)))
        if len(results) >= limit:
            links += "<a href='{}'>&gt;&gt;Next Page</a>".format(
                escape(link_format.format(page + 1)))
    elif mode == "more":
        links = "<a href='/matches/page/0'>Show More</a>"
    elif mode == "none":
        links = None
    else:
        raise ValueError("Not undertood value for mode: " + repr(mode))

    if links is not None:
        link_layout = "<div class='link-container'>{}</div>".format(links)
    else:
        link_layout = ""

    return result + link_layout


def render_match(dbhandler, id):
    cursor = dbhandler.get_cursor()
    with cursor:
        cursor.execute(
            """
        SELECT
            matches.id,
            ai1.name,
            ai2.name,
            ai1.id,
            ai2.id,
            money_0,
            money_1,
            ai_1_loan,
            ai_2_loan,
            company_value_1,
            company_value_2,
            preset,
            phase,
            image_name,
            save_name,
            winner,
            ai_1_total_vehicles,
            ai_2_total_vehicles,
            ai_1_road_vehicles,
            ai_2_road_vehicles,
            ai_1_trains,
            ai_2_trains,
            ai_1_ships,
            ai_2_ships,
            ai_1_planes,
            ai_2_planes,
            ai_1_map_control,
            ai_2_map_control,
            map_control_stations_1,
            map_control_stations_2,
            map_control_road_1,
            map_control_road_2,
            map_control_rail_1,
            map_control_rail_2,
            ai_1_log,
            ai_2_log,
            pending_match_id
        FROM matches
            INNER JOIN ai as ai1
                ON ai1.id = matches.opponent_0
            INNER JOIN ai as ai2
                ON ai2.id = matches.opponent_1
        WHERE matches.id=(%s)""",
            (id,)
        )

        row = cursor.fetchone()
        if row is None:
            return None

        (match_id, ai_1_name,
         ai_2_name,
         ai_1_id, ai_2_id,
         ai1_money, ai2_money,
         ai_1_loan, ai_2_loan,
         ai_1_value,
         ai_2_value,
         preset, phase, image_name, save_name, winner,
         ai_1_total_vehicles, ai_2_total_vehicles,
         ai_1_road_vehicles, ai_2_road_vehicles,
         ai_1_trains,
         ai_2_trains,
         ai_1_ships,
         ai_2_ships,
         ai_1_planes,
         ai_2_planes,
         ai_1_map_control,
         ai_2_map_control,
         ai_1_map_stations,
         ai_2_map_stations,
         ai_1_map_road,
         ai_2_map_road,
         ai_1_map_rail,
         ai_2_map_rail,
         ai_1_log,
         ai_2_log,
         pending_match_id
         ) = row

        cursor.execute("""
        SELECT id, phase FROM
        matches
        WHERE pending_match_id IS NOT NULL and pending_match_id=(%s)
        """, (pending_match_id,))
        similar_matches = cursor.fetchall()

        title_markup = (render_title_with_back(
            '<a href="/ai/{}">{}</a> vs <a href="/ai/{}">{}</a>'.format(
                ai_1_id,
                escape(ai_1_name),
                ai_2_id,
                escape(ai_2_name)
            ), False))
        subtitle_markup = (
                '<div class="match-subtitle"><div>Preset: ' +
                '<a href="/phase/' + str(phase) + '" class="match-sub-value">' + str(escape(preset)) + '</a></div>' +
                '<div>Year: '
                '<a href="/phase/' + str(phase) + '" class="match-sub-value">' + str(phase) + '</a></div></div>')

        if len(similar_matches) != 0:
            similar_matches_markup = '<h3>This game in different eras</h3>' + render_timeline(
                [(i[1], '/match/' + str(i[0])) for i in similar_matches]
            )
        else:
            similar_matches_markup = '<p>This game is too old to store data about different matches</p>'

        stats = (
            ('Total Money', (ai1_money, ai2_money), '£{:,}', True),
            ('Cash', (
                ai1_money + ai_1_loan - ai_1_value, ai2_money + ai_2_loan - ai_2_value), '£{:,}'),
            ('Loan', (ai_1_loan, ai_2_loan), '£{:,}'),
            ('Vehicle Value', (ai_1_value, ai_2_value), '£{:,}'),
            ('Total Vehicles', (ai_1_total_vehicles, ai_2_total_vehicles), '{:,}', True),
            ('road vehicles', (ai_1_road_vehicles, ai_2_road_vehicles)),
            ('trains', (ai_1_trains, ai_2_trains), '{:,}'),
            ('ships', (ai_1_ships, ai_2_ships)),
            ('planes', (ai_1_planes, ai_2_planes)),
            ('Map Control', (ai_1_map_control, ai_2_map_control), '{:,} Tiles', True),
            ('station tiles', (ai_1_map_stations, ai_2_map_stations), '{:,} Tiles'),
            ('road tiles', (ai_1_map_road, ai_2_map_road), '{:,} Tiles'),
            ('rail tiles', (ai_1_map_rail, ai_2_map_rail), '{:,} Tiles'),
            ('other tiles',
             (ai_1_map_control -
              ai_1_map_stations - ai_1_map_road - ai_1_map_rail,
              ai_2_map_control - ai_2_map_stations - ai_2_map_road - ai_2_map_rail),
             '{:,} Tiles')
        )

    def markup_single_stat(name, values, unit='{:,}', header=False):
        if (values[0] == -1 and values[1] == -1):
            return ''
        return ('<tr class="' + ('category' if header else 'no-category') +
                '"><th scope="row">{}</th><td class="{}">{:}</td><td class="{}">{:}</td></tr>'.format(
                    escape(name),
                    'tbl-grey' if values[0] == 0 else '',
                    escape(unit.format(values[0])),
                    'tbl-grey' if values[1] == 0 else '',
                    escape(unit.format(values[1]))
                ))

    stats_markup = (
            '<h2>Statistics</h2>'
            '<table cellspacing="0" class="simple category-table"><tr><th></th><th scope="col">' +
            str(escape(ai_1_name)) +
            '</th><th scope="col">' + str(escape(ai_2_name)) + '</th></tr>' +
            '\n'.join(
                markup_single_stat(*i)
                for i in stats
            ) + '</table>')

    image_markup = """
        <h3>Ownership Map</h3>
        <div class="image-container">
            <img src="/{0}">
            <div class="image-subcontainer">
                <div>
                    <div class="red-rect">
                    </div>
                    {1}
                </div>
                <div>
                    <div class="dark-red-rect">
                    </div>
                    {1} &mdash; Stations
                </div>
                <div>
                    <div class="green-rect">
                    </div>
                    {2}
                </div>
                <div>
                    <div class="dark-green-rect">
                    </div>
                    {2} &mdash; Stations
                </div>
            </div>
        </div>""".format(
        escape(image_name),
        escape(ai_1_name),
        escape(ai_2_name)
    )

    files = [
        ('Save File', save_name)
    ]
    if ai_1_log is not None:
        files.append((ai_1_name + " log", ai_1_log))
    if ai_2_log is not None:
        files.append((ai_2_name + " log", ai_2_log))

    save_markup = (
        '''<h2>Downloads</h2>
        <div class="download-container">
    {}
        </div>'''.format(
            ''.join(
                '''<a class="download" href="/{}" rel="download">
                {}
                </a>'''.format(
                    i[1],
                    i[0]
                )
                for i in files
            )
        )

    )

    # similar_matches_markup = (
    #        '<h2>This game at different Times</h2>' +
    #         ''.join(
    #             '<a href="/match/' + str(escape(i[0])) + '">' +
    #             str(escape(i[1])) + '</a>'
    #             for i in similar_matches
    #         )
    # )

    return build_layout(
        title_markup +
        subtitle_markup +
        similar_matches_markup +
        image_markup +
        stats_markup +
        save_markup,

        title=ai_1_name + ' vs ' + ai_2_name
    )


def render_ai_page(cursor, ai_id, page=0, filter=None):
    if not isinstance(page, int):
        return """
<div>
    <h1>Error</h1>
    <p>Invalid page number
</div>
        """
    cursor.execute("""
    SELECT name
    FROM ai
    WHERE id=%s
    """,
                   (ai_id,))

    result = cursor.fetchone()
    if result is None:
        return None
    ai_name = result[0]

    header = render_title_with_back(ai_name)

    matches = render_matches(cursor, page=page, limit=10, mode="page", ai=ai_id,
                             link_format="/ai/{}/page/{{}}#match".format(ai_id),
                             empty_matches="This AI has not yet played any matches",
                             filter=filter)

    graph = render_ai_graph(cursor, ai_id)

    if filter is None or filter == '' or filter == 'none' or filter == {}:
        filter_button = '<a href="/ai/{}/page/{}/filter/phase=2025">Show only 2025</a>'.format(ai_id, page)
    else:
        filter_button = '<a href="/ai/{}/page/{}">Show all</a>'.format(ai_id, page)

    filter_button = '<div class="link-container">' + filter_button + '</div>'

    return build_layout(
        header +
        '<h3>Relative ranking for different eras</h3>' +
        graph +
        '<h2 id="match">Matches for this AI</h2>' +
        filter_button +
        matches,
        title=ai_name + ('' if page == 0 else ' page {}'.format(page + 1)),
        canonical='/ai/{}'.format(ai_id)
    )


def render_ai_graph(cursor, ai, preset="default"):
    size = (400, 150)
    x_guides = (-150, -100, -50, 0, 50, 100, 150)
    y_guides = (1980, 1990, 2000, 2010, 2020)
    range_factor = (1974, 2027), (155, -155)

    def scale_value(value, size, range_coord):
        return size * (value - range_coord[0]) / (range_coord[1] - range_coord[0])

    cursor.execute(
        """
        SELECT phase, elo
        FROM elos
        WHERE ai=%s
        AND preset=%s
        ORDER BY phase
    """,
        (ai, preset))

    elos = cursor.fetchall()

    points = [tuple(
        scale_value(coord, s, rng)
        for coord, s, rng in zip(j, size, range_factor))
        for j in elos
    ]

    def render_data_point(x, y):
        return "{} {}".format(x, y)

    horizontal_grid_markup = (
        """<path fill="none" stroke="#e6f2fb" stroke-width="0.5" d="{}"/>""".format(
            "".join(
                "M0 {0}L{1} {0}".format(
                    scale_value(i, size[1], range_factor[1]),
                    size[0])
                for i in x_guides))
    )
    vertical_grid_markup = (
        """<path fill="none" stroke="#e6f2fb" stroke-width="0.5" d="{}"/>""".format(
            "".join(
                "M{0} 0 L{0} {1}".format(
                    scale_value(i, size[0], range_factor[0]),
                    size[1])
                for i in y_guides))
    )

    horizontal_labels = (
        "".join(
            """<text x="{}" y="{}" font-size="5" fill="grey">{}</text>""".format(
                scale_value(i, size[0], range_factor[0]) + 3,
                size[1] - 3,
                str(i)
            )
            for i in y_guides
        )
    )

    vertical_labels = (
        "".join(
            """<text x="{}" y="{}" font-size="5" fill="grey">{:,}</text>""".format(
                3,
                scale_value(i, size[1], range_factor[1]) - 3,
                i + 1000
            )
            for i in x_guides
        )
    )

    dot_markup = (
        "".join(
            """<circle cx={0} cy={1} r="2.5"
             stroke="#003082"
             stroke-width="1.5"
            />
            <g transform="translate({0} {1})">
                <rect fill="#fff0" width="20" height="20" x="-10" y="-5"/>
                <path class="main"
                    d="M 0 4 L 2 6 L 10 6 L 10 15 L -10 15 L -10 6 L -2 6 Z"/>
                <text font-size="5" x="0" y="13" text-anchor="middle">{2:,}</text>
            </g>
            """.format(
                scale_value(i[0], size[0], range_factor[0]),
                scale_value(i[1], size[1], range_factor[1]),
                i[1] + 1000
            )
            for i in elos
        )
    )

    return '''
    <svg viewbox="0 0 {0} {1}">
        <style>
            circle {{
                fill: white;
            }}
            
            g {{
                fill: #fff1;
            }}
            
            g:hover path.main {{
                fill: #003082;
            }}
            
            g:hover text {{
                fill: white;
            }}
        </style>
        {2}
        <path fill="none" stroke="#003082" stroke-width="2" d="{3}"/>
        {4}
    </svg>
    '''.format(
        size[0],
        size[1],
        horizontal_grid_markup + vertical_grid_markup,
        "M" + "L".join(render_data_point(*i) for i in points),
        horizontal_labels + vertical_labels + dot_markup,
    )


def render_home_promo():
    return """
        <h1>OpenTTD AI Rankings</h1>
        <p>This site attempts to determine which AI's for openTTD perform the best, by running
        many matches and computing scores based on the ELO ranking system. Additionally, statistics
        are collected about every match, in order to better analyze AI behavior.</p>
        <p>Games are played from 1975 to 2025. You can watch the active game by joining
        <a href="openttd://openttd.mousetail.nl">openttd.mousetail.nl</a>, only spectating is possible.
        </p>
        <p>A AI wins by having the most total money (cash - loan) at the end of a specified period.
        Separate rankings are calculated for different amounts of years. One AI might perform better
        after one year, another gets the optimal profit after 20.</p>
        <div class="link-container"><a href="/static/doc/about.html">More Info</a></div>
        """
