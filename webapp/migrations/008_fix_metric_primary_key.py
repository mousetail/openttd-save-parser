def migrate(cursor):
    cursor.execute(
        """
        ALTER TABLE metrics DROP CONSTRAINT metrics_pkey
        """
    )

    cursor.execute(
        """
        ALTER TABLE metrics ADD PRIMARY KEY (match, metric, preset)
        """
    )


def revert(cursor):
    pass
