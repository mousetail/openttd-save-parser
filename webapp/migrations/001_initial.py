def migrate(cursor):
    cursor.execute("""
                CREATE TABLE IF NOT EXISTS ai(
                    id SERIAL PRIMARY KEY,
                    name VARCHAR(255) NOT NULL UNIQUE
                )
            """)

    cursor.execute("""
               CREATE TABLE IF NOT EXISTS pending_matches(
                id SERIAL PRIMARY KEY,
                date TIMESTAMP DEFAULT now(),
                opponent_1 INTEGER NOT NULL REFERENCES ai(id),
                opponent_2 INTEGER NOT NULL REFERENCES ai(id),
                preset VARCHAR(128) NOT NULL
               );
            """)

    cursor.execute("""
                CREATE TABLE IF NOT EXISTS matches(
                    id SERIAL PRIMARY KEY,
                    date TIMESTAMP DEFAULT now(),
                    preset VARCHAR(128) NOT NULL,
                    phase INTEGER NOT NULL,
                    winner INTEGER NOT NULL,
                    image_name VARCHAR(255) NOT NULL,
                    save_name VARCHAR(255) NOT NULL,
                    opponent_0 INTEGER NOT NULL REFERENCES ai(id),
                    opponent_1 INTEGER NOT NULL REFERENCES ai(id),
                    elo_0 INTEGER NOT NULL,
                    elo_1 INTEGER NOT NULL,
                    money_0 INTEGER NOT NULL,
                    money_1 INTEGER NOT NULL
                );
            """)

    cursor.execute("""
                CREATE TABLE IF NOT EXISTS elos (
                    id SERIAL PRIMARY KEY,
                    ai INTEGER REFERENCES ai(id) NOT NULL,
                    preset VARCHAR(128) NOT NULL,
                    phase INTEGER NOT NULL,
                    elo INTEGER NOT NULL DEFAULT(0)
                )
            """)

    cursor.execute("""SELECT count(*) FROM ai""")
    if cursor.fetchone()[0] <= 0:

        ais = ["AI Name", "AdmiralAI", "Convoy", "PathZilla", "StreetTraffic1", "Denver & Rio Grande", "PAXLink",
               "rocketAI", "WrightAI", "Chopper", "MogulAI", "AroAI", "Idle", "TownCars", "IdleMore",
               "Rondje om de kerk", "gelignAIte", "Beginner Tutorial - Ship AI", "NoCAB", "TutorialAI",
               "NoCAB - Bleeding Edge edition", "Rythorn Airline AI", "MailAI", "CPU", "CityConnecter", "Terron AI",
               "FastPTPAI", "EpicTrans", "SynTrans", "trAIns", "TREE", "TeshiNet", "Orders Assistant AI", "BorkAI",
               "AIAI", "DictatorAI", "TracAI", "RoadRunner", "ChooChoo", "CluelessPlus", "WmDOT", "LuDiAI", "MpAI",
               "DumbAI", "EmotionAI", "OtviAI", "RoadAI", "SimpleAI", "SnakeAI", "CivilAI", "IdleMoreMore", "Mungo",
               "FanAI", "SmallTownAI", "NoNoCAB", "Trans AI", "RailwAI", "WormAI", "LuDiAI AfterFix", "KrakenAI",
               "MedievalAI", "ShipAI", "HeliFerry"]
        for ai in ais:
            cursor.execute("INSERT INTO ai(name) VALUES (%s)", (ai,))
