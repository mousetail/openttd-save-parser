def migrate(cursor):
    cursor.execute(
        """
        ALTER TABLE elos
        ADD COLUMN metric VARCHAR(64) NOT NULL DEFAULT 'money'
        """)

    cursor.execute(
        """
        ALTER TABLE metrics ADD COLUMN preset VARCHAR(128) DEFAULT ('default')
        """
    )


def revert(cursor):
    pass
