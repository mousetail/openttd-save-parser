def migrate(cursor):
    cursor.execute("""
    ALTER TABLE matches
        ADD COLUMN
            company_value_1 INTEGER NOT NULL DEFAULT(0),
        ADD COLUMN
            company_value_2 INTEGER NOT NULL DEFAULT(0),
        ADD COLUMN
            map_control_stations_1 INTEGER NOT NULL DEFAULT(0),
        ADD COLUMN
            map_control_stations_2 INTEGER NOT NULL DEFAULT(0),
        ADD COLUMN
            map_control_road_1 INTEGER NOT NULL DEFAULT(0),
        ADD COLUMN
            map_control_road_2 INTEGER NOT NULL DEFAULT(0),
        ADD COLUMN
            map_control_rail_1 INTEGER NOT NULL DEFAULT(0),
        ADD COLUMN
            map_control_rail_2 INTEGER NOT NULL DEFAULT(0)
        """)

def revert(cursor):
    pass