def migrate(cursor):
    cursor.execute("""
    ALTER TABLE matches
        ADD COLUMN
            pending_match_id INTEGER REFERENCES pending_matches(id),
        ADD COLUMN
            ai_1_map_control INTEGER NOT NULL DEFAULT(-1),
        ADD COLUMN
            ai_2_map_control INTEGER NOT NULL DEFAULT(-1),
        ADD COLUMN
            ai_1_total_vehicles INTEGER NOT NULL DEFAULT(-1),
        ADD COLUMN
            ai_2_total_vehicles INTEGER NOT NULL DEFAULT(-1),
        ADD COLUMN 
            ai_1_road_vehicles INTEGER NOT NULL DEFAULT(-1),
        ADD COLUMN
            ai_2_road_vehicles INTEGER NOT NULL DEFAULT(-1),
        ADD COLUMN
            ai_1_trains INTEGER NOT NULL DEFAULT(-1),
        ADD COLUMN
            ai_2_trains INTEGER NOT NULL DEFAULT(-1),
        ADD COLUMN
            ai_1_planes INTEGER NOT NULL DEFAULT(-1),
        ADD COLUMN
            ai_2_planes INTEGER NOT NULL DEFAULT(-1),
        ADD COLUMN
            ai_1_ships INTEGER NOT NULL DEFAULT(-1),
        ADD COLUMN
            ai_2_ships INTEGER NOT NULL DEFAULT(-1),
        ADD COLUMN
            ai_1_loan INTEGER NOT NULL DEFAULT(0),
        ADD COLUMN
            ai_2_loan INTEGER NOT NULL DEFAULT(0)
        """)

def revert(cursor):
    cursor.execute("""
    ALTER TABLE matches
    DROP COLUMN pending_match_id
    """)