def migrate(cursor):
    cursor.execute("""
    ALTER TABLE matches
        DROP COLUMN elo_0,
        DROP COLUMN elo_1
    """)

    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS metrics(
            match INTEGER NOT NULL REFERENCES matches(id),
            metric VARCHAR(64) NOT NULL,
            ai_1 INTEGER NOT NULL REFERENCES ai(id),
            ai_2 INTEGER NOT NULL REFERENCES ai(id),
            value_1 INTEGER NOT NULL,
            value_2 INTEGER NOT NULL,
            elo_1 INTEGER NOT NULL,
            elo_2 INTEGER NOT NULL,
            primary key (match, metric)
        )
        """
    )


def revert(cursor):
    pass
