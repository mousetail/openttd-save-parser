import uuid

def migrate(cursor):
    cursor.execute(
        """
        CREATE TABLE authenticated_instances (
            id SERIAL NOT NULL PRIMARY KEY,
            authentication_code VARCHAR(36) NOT NULL UNIQUE,
            name varchar NOT NULL UNIQUE
        )
        """
    )

    cursor.execute(
        """
        INSERT INTO authenticated_instances (authentication_code, name)
        VALUES (%s, 'unknown')
        """,
        (str(uuid.uuid4()),)

    )

    cursor.execute("""
    ALTER TABLE matches
        ADD COLUMN runner INTEGER NOT NULL REFERENCES authenticated_instances(id) DEFAULT 1
    """)


def revert(cursor):
    pass
