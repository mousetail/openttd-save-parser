from flask import request
import functools


def parse_filter_string(filter_string: str):
    if filter_string is None or filter_string == '' or filter_string.lower() == 'none':
        return {}
    elif filter_string.count('+') != filter_string.count('=') - 1:
        return {}
    else:
        return {item.split('=')[0]: item.split('=')[1] for item in filter_string.split('+')}


def check_authentication(db):
    def _check_authentication(funk):
        @functools.wraps(funk)
        def _check_authentication_inner(*params, **kwargs):
            with db.get_cursor() as cursor:
                authentication_key = request.headers.get('x-authentication-key')
                cursor.execute("""
                    SELECT id FROM
                    authenticated_instances
                    WHERE authentication_code=%s
                """, (authentication_key,))
                if cursor.fetchone() is not None:
                    return funk(*params, **kwargs)
                else:
                    return "Not authorized", 403
        return _check_authentication_inner
    return _check_authentication
