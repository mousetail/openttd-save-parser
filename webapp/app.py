import json
import os
import uuid
import random
import shutil
import subprocess
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

from flask import Flask, redirect, Response, request, escape, send_from_directory

from . import util
from . import db
from .layouts import *
from .constants import *

with open('config.json') as f:
    config = json.load(f)
sentry_sdk.init(
    config['sentry_url'],
    integrations=[FlaskIntegration()]
)
app = Flask(__name__)

dbhandler = db.DBHandler()


@app.route('/')
def home():
    with dbhandler.get_cursor() as cursor:
        text = (render_home_promo() +
                '<h2 title="matches">Recent Matches</h2>' +
                render_matches(cursor, 0, 5,
                               mode="more") +
                '<h2 id="ranking">AI Rankings</h2>' +
                render_ais(
                    cursor, 2025))

    return build_layout(text)


@app.route('/matches/page/<int:page>')
def matches(page=0):
    with dbhandler.get_cursor() as cursor:
        return build_layout(
            render_title_with_back("Matches") +
            render_matches(cursor, page),
            title='Matches Page {}'.format(page + 1)
        )


@app.route('/ranking/phase/<int:phase>/preset/<preset>')
def ranking(phase=2050, preset="default"):
    with dbhandler.get_cursor() as cursor:
        return build_layout(
            render_title_with_back("Rankings for " + str(phase)) +
            render_ais(
                cursor,
                phase,
                preset
            ),
            title='Ranking for {}'.format(preset)
        )


@app.route('/match/<int:match_id>')
def match(match_id):
    result = render_match(dbhandler, match_id)
    if (result):
        return result
    else:
        return Response(
            build_layout(
                render_title_with_back("404 No such Match") +
                "<p>Match not found</p>",
                title='404'),
            status=404)


@app.route('/ai/<int:ai_id>')
@app.route('/ai/<int:ai_id>/filter/<string:filter>')
@app.route('/ai/<int:ai_id>/page/<int:page>')
@app.route('/ai/<int:ai_id>/page/<int:page>/filter/<string:filter>')
def ai(ai_id, page=0, filter=None):
    with dbhandler.get_cursor() as cursor:
        result = render_ai_page(cursor, ai_id, page=page, filter=util.parse_filter_string(filter))
        if result:
            return result
        else:
            Response(build_layout(
                render_title_with_back("404 No such AI") +
                "<p>An AI With the specified ID could not be found</p>",
                title='404'),
                status=404,
            )


@app.route('/api/start_match', methods=["POST"])
@util.check_authentication(dbhandler)
def start_match():
    cursor = dbhandler.get_cursor()
    cursor.execute("SELECT id, name FROM ai;")
    ais = cursor.fetchall()
    ai1 = ai2 = None

    preset = random.choice(presets)

    while ai1 == ai2:
        ai1 = random.choice(ais)
        ai2 = random.choice(ais)

    def ai_to_str(ai):
        return {
            "name": ai[1],
            "id": ai[0]
        }

    cursor.execute("INSERT INTO pending_matches(opponent_1, opponent_2, preset) VALUES (%s, %s, %s) RETURNING id",
                   (ai1[0], ai2[0], preset))
    result = cursor.fetchone()[0]
    cursor.connection.commit()
    cursor.close()

    return Response(
        json.dumps({
            "ai1": ai_to_str(ai1),
            "ai2": ai_to_str(ai2),
            "preset": preset,
            "start_year": 1975,
            "phases": phases,
            "id": result,
        }),
        mimetype='application/json')


@app.route('/api/submit_results/id/<int:match_id>/phase/<int:phase>/preset/<string:preset>', methods=['POST'])
@util.check_authentication(dbhandler)
def submit_results(match_id, phase, preset):
    save_data = request.stream

    first_3_bytes = save_data.read(3)

    if first_3_bytes != b'OTT':
        return Response('Invalid OpenTTD save format', status=400)

    save_name = os.path.join(config["saves_location"], str(uuid.uuid4()) + ".sav")
    with open(save_name, 'wb') as f:
        f.write(first_3_bytes)
        shutil.copyfileobj(save_data, f)

    cursor = dbhandler.get_cursor()
    cursor.execute(
        """
        INSERT INTO matches(
            preset, phase, pending_match_id,
            opponent_1,
            opponent_0,
            winner,
            money_0,
            money_1,
            image_name,
            save_name)
        VALUES (
            %s, %s, %s,
            1, 1,
            0,
             -1, -1,
             '', ''
        )
        RETURNING id""",
        (preset, phase, match_id)
    )

    id, = cursor.fetchone()
    cursor.connection.commit()

    subprocess.Popen(
        [config["python_location"],
         config["saveparser_location"],
         save_name,
         config["images_location"],
         str(match_id),
         str(preset),
         str(phase),
         str(id)
         ])

    return "Yess"


@app.route(
    '/api/submit_logs/id/<int:pending_match_id>/phase/<int:phase>/preset/<string:preset>/ai/<int:ai>',
    methods=["POST"])
@util.check_authentication(dbhandler)
def submit_logs(pending_match_id, phase, preset, ai):
    with dbhandler.get_cursor() as cursor:
        cursor.execute("""
        SELECT id, preset
        FROM matches
        WHERE pending_match_id=%s AND
            phase=%s
        """, (pending_match_id, phase))

        match_id, new_preset = cursor.fetchone()
        if new_preset != preset:
            return Response('Invalid match', status=400)

        log_file = str(uuid.uuid4()) + '.log'
        log_path = os.path.join(config["logs_location"], log_file)
        with open(log_path, 'wb') as f:
            shutil.copyfileobj(request.stream, f)

        log_attribute = "ai_1_log" if ai == 1 else "ai_2_log"

        # Find all old AI's so we can delete them
        cursor.execute(
            """
        SELECT {0}
        FROM matches
        WHERE pending_match_id=%s AND {0} IS NOT null
        """.format(log_attribute),
            (pending_match_id,)
        )
        for filename, in cursor.fetchall():
            if filename and filename.startswith(config["logs_location"]):
                os.unlink(filename)
                # Should Delete

        # Basically, all matches with the same pending_match_id should always have
        # The same log for very ai
        cursor.execute(
            """
            UPDATE matches
            SET {0}=%s
            WHERE pending_match_id=%s""".format(log_attribute),
            ('logs/' + log_file, pending_match_id)
        )

        cursor.connection.commit()
    return 'yesss'


@app.errorhandler(404)
def handle_404_error(page=None):
    return build_layout(
        render_title_with_back("404 Page not found") +
        "<p>The specified page could not be loaded</p>",
        title='404'
    ), 404


@app.errorhandler(500)
def handle_500_error(page=None):
    return build_layout(
        render_title_with_back("Internal Error") +
        "<p>There was an error displaying this page. Please try again later</p>",
        title='500'
    ), 500


@app.route('/saves/<string:filename>')
def download_save(filename):
    return send_from_directory('saves', filename)


@app.route('/images/<string:filename>')
def download_image(filename):
    return send_from_directory('images', filename)


@app.route('/logs/<string:filename>')
def download_logs(filename):
    return send_from_directory('logs', filename)


@app.route('/favicon.ico')
def redirect_to_favicon():
    return redirect('/static/images/logo.png')


if __name__ == "__main__":
    app.run(port=5000)
