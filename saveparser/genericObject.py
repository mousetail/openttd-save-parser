class GenericObject(object):
    def __repr__(self):
        return (self.__class__.__name__ + "(" +
                ", ".join(key + "=" + str(value) for key, value
                          in self.__dict__.items()
                          if not key.startswith("_")
                          and not hasattr(value, "__call__")) +
                ")")
