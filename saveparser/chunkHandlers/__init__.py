from saveparser.chunkHandlers import aiHandler
from . import gamelogHandlers, mapHandler, miscHandler, vehicleHandler, orderHandlers, stationHandlers, townHandlers, \
    depotHandler, engineHandlers, companyHandler, \
    newGrfHandlers
from typing import List


def get_all_handlers():
    return [
        gamelogHandlers.GamelogHandler(),
        mapHandler.MapSizeHandler(),
        mapHandler.MapTypeHandler(),
        mapHandler.MapHeightHandler(),
        mapHandler.MapOwnerHandler(),
        mapHandler.MapGeneral2Handler(),
        mapHandler.MapGeneral3Handler(),
        mapHandler.MapGeneral4Handler(),
        mapHandler.MapGeneral5Handler(),
        mapHandler.MapGeneral6Handler(),
        mapHandler.MapGeneral7Handler(),
        miscHandler.DateHandler(),
        miscHandler.ViewHandler(),
        miscHandler.CheatsHandler(),
        vehicleHandler.VehicleHandler(),
        depotHandler.DepotHandler(),
        orderHandlers.BackupOrderHandlers(),
        orderHandlers.OrderHandler(),
        orderHandlers.OrderListHandler(),
        engineHandlers.EngineHandler(),
        townHandlers.TownHandler(),
        townHandlers.SignHandler(),
        stationHandlers.StationHandler(),
        companyHandler.CompanyHandler(),
        newGrfHandlers.NewGRFHandler(),
        aiHandler.AIHandler(),
    ]


def get_ignored_handlers() -> List[bytes]:
    return [
        b'MAP8',  # Map

        b'PATS',
        b'INDY',  # Industries
        b'IIDS',
        b'TIDS',
        b'IBLD',
        b'ITBL',

        b'CAPY',  # Economy
        b'PRIC',
        b'CAPR',
        b'ECMY',
        b'SUBS',  # Subsidy

        b'CMDL',  # Story/goal
        b'CMPU',
        b'GOAL',
        b'STPE',
        b'STPA',

        b'EIDS',  # Legacy Components
        b'HIDS',  # NewGRF town mapping

        b'ROAD',  # Bus and truck stops

        # b'AIPL',  # AI Data
        b'GSTR',  # Game Script String
        b'GSDT',  # Game Script Data

        b'ANIT',  # Animated Tile
        b'GRPS',  # Vehicle Groups
        b'CAPA',  # Cargo Packets
        b'ERNW',  # Autoreplace
        b'RAIL',  # Rail type labels

        b'LGRP',  # Link Graph
        b'LGRJ',  # Link Graph Job
        b'LGRS',  # Link Graph Schedule

        b'ATID',  # Airport newGRF mapping
        b'APID',  # Airporttile newGRF mapping
        b'OBID',  # Object NewGRF mapping
        b'OBJS',  # Objects

        b'PSAC',  # Persistent Storage
    ]
