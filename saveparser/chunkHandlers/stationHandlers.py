from .chunkHandler import ChunkHandler
from saveparser.compression import Reader
from saveparser.gameState import GameState
from saveparser.gameState.dateState import DateState


class StationHandler(ChunkHandler):
    FACIL_WAYPOINT = 128

    def __init__(self):
        super().__init__(b'STNN')

    def load(self, f: Reader, gs: GameState):
        for index, length in self.iterate_array(f, self.CH_ARRAY):
            start_postion = f.tell()

            station_type = f.read_byte()
            if (station_type & self.FACIL_WAYPOINT) != 0:
                # Waypoint
                pass
            else:
                xy = f.read_uint()
                town = f.read_uint()
                string_id = f.read_ushort()
                name = f.read_string()

                delete_ctr = f.read_ubyte()
                owner = f.read_ubyte()
                facilities = f.read_ubyte()
                build_date = f.read_uint()

                print("station town: " + str(town)
                      + " date " + str(DateState.int_to_date(build_date)))
                if (name):
                    print("station name: " + str(name))

                f.read(4)

                # End base station

            f.seek(start_postion + length)
