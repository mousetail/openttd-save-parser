import struct
import abc

from saveparser import genericObject
from saveparser.compression import Reader
from saveparser.gameState import GameState


class SaveLoadParams(genericObject.GenericObject):
    SL_VAR = 0
    SL_REF = 1
    SL_ARR = 2
    SL_STR = 3
    SL_LST = 4
    SL_END = 15

    SLE_FILE_I8 = 0,
    SLE_FILE_U8 = 1,
    SLE_FILE_I16 = 2,
    SLE_FILE_U16 = 3,
    SLE_FILE_I32 = 4,
    SLE_FILE_U32 = 5,
    SLE_FILE_I64 = 6,
    SLE_FILE_U64 = 7,
    SLE_FILE_STRINGID = 8,  # StringID offset into strings - array
    SLE_FILE_STRING = 9

    def load(self, f):
        raise NotImplementedError()


class SaveLoadInt(SaveLoadParams):
    def __init__(self, glob, command, f_signed, f_size, length):
        self.glob = glob
        self.command = command
        self.f_signed = f_signed
        self.f_size = f_size
        self.length = length
        assert f_size != 3 and 4 >= f_size >= 0, "invalid length"

    def load(self, f):
        fmt = 'bh i'
        if not self.f_signed:
            fmt = fmt.upper()
        fmt = fmt[self.f_size - 1]
        # print ("fmt: "+repr(fmt))
        return struct.unpack(fmt * self.length, f.read(self.f_size * self.length))


class SaveLoadParamsVar(SaveLoadInt):
    def __init__(self, signed, size, length):
        super(SaveLoadParams, self).__init__(
            False,
            SaveLoadParams.SL_VAR,
            signed,
            size,
            length
        )


class SaveLoadNullTermString(SaveLoadParams):
    def __init__(self, glob=False, command=0):
        self.glob = glob
        self.command = command

    def load(self, f):
        bf = b''
        while not bf.endswith(b'\x00'):
            bf += f.read(1)
        return bf


class SaveLoadPascalString(SaveLoadParams):
    def __init__(self, glob=False, command=0):
        self.glob = glob
        self.command = command

    def get_length(self, f):
        bit = ord(f.read(1))
        if bit & 1 << 7:
            bit &= ~0x80
            if bit & 1 << 6:
                bit &= ~0x40
                if bit & 1 << 5:
                    bit &= ~0x20
                    if bit & 1 << 4:
                        bit &= ~0x10
                        if bit & 1 << 3:
                            raise ValueError("bit 3 should not be set")
                        bit = f.read(1)
                    bit = bit << 8 | f.read(1)
                bit = bit << 8 | f.read(1)
            bit = bit << 8 | f.read(1)
        return bit

    def load(self, f):
        return f.read(self.get_length(f))


class ChunkHandler(object, metaclass=abc.ABCMeta):
    CH_SPARSE_ARRAY = 2
    CH_ARRAY = 1

    MAX_COMPANIES = 15
    NUM_TE = 6

    def __init__(self, key: bytes):
        self.id = key

        self.next_offset = 0

    def load_objects(self, sl_params, f):
        return [i.load(f) for i in sl_params]

    def load_object(self, sl_param, f):
        return sl_param.load(f)

    def load_string(self, f):
        return SaveLoadNullTermString().load(f)

    def iterate_array(self, f: Reader, array_type):
        # Note: This assignes to external variables
        # _sl.obj_len: The length of the object
        # _next_offs: ??
        index = 0
        while True:
            length = f.read_gamma()
            if length == 0:
                self.next_offset = 0
                return -1

            length = length - 1  # 0 means nothing, so compensate

            self.next_offset = f.tell() + length

            pre_gamma = f.tell()
            if array_type == self.CH_SPARSE_ARRAY:
                index = f.read_gamma() + 1
            else:
                index = index + 1
            object_length = length + pre_gamma - f.tell()

            if length != 0:
                yield (index - 1, object_length)
                assert f.tell() == self.next_offset, (
                    f.tell(), self.next_offset, object_length
                )

    @abc.abstractmethod
    def load(self, f: Reader, gs: GameState):
        return NotImplemented
