from saveparser.chunkHandlers.chunkHandler import ChunkHandler
from saveparser.compression import Reader
from saveparser.gameState import GameState


class EngineHandler(ChunkHandler):
    def __init__(self):
        super().__init__(b'ENGN')

    def load(self, f: Reader, gs: GameState):
        for elem in self.iterate_array(f, self.CH_ARRAY):
            intro_date = f.read_uint()
            age = f.read_int()
            reliability = f.read_ushort()
            f.read(8)
            duration = f.read_ushort()
            f.read(4)

            flags = f.read_byte()
            f.read(4)
            company_avail = f.read_ushort()
            company_hidden = f.read_ushort()

            name = f.read_string()
            if name:
                print(name)
