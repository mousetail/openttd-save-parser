#from . import chunkHandler
from saveparser.gameState import grfState
from saveparser import genericObject


class GrfHandler(genericObject.GenericObject):
    @staticmethod
    def load_grf_id(f):
        return grfState.GrfID(f.read(4), f.read(16))