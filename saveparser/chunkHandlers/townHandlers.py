from .chunkHandler import ChunkHandler
from saveparser.compression import Reader
from saveparser.gameState import GameState


class TownHandler(ChunkHandler):
    def __init__(self):
        super().__init__(b'CITY')

    def load(self, f: Reader, gs: GameState):
        for index, length in self.iterate_array(f, self.CH_ARRAY):
            file_position = f.tell()

            position = f.read_uint()

            f.read(10)  # New GRF data

            name = f.read_string()
            if name:
                print("town name: " + str(name))

            f.read(3)  # flags, statues

            f.read(2 + 2 * self.MAX_COMPANIES)  # ratings
            f.read(2 * 2 * self.MAX_COMPANIES)  # Bribe attempts

            f.read(4 * self.NUM_TE)

            text = f.read_string()
            if text:
                print(text)

            f.seek(file_position + length)


class SignHandler(ChunkHandler):
    def __init__(self):
        super().__init__(b'SIGN')

    def load(self, f: Reader, gs: GameState):
        for index, length in self.iterate_array(f, self.CH_ARRAY):
            name = f.read_string()
            x, y = f.read_int(), f.read_int()
            owner = f.read_ubyte()
            z = f.read_int()

            #if name:
                #print("Sign " + str(name)+" pos: "+str(x)+",",
                #      str(y)+","+str(z))
