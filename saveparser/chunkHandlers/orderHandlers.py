from .chunkHandler import ChunkHandler
from saveparser.compression import Reader
from saveparser.gameState import GameState


class BackupOrderHandlers(ChunkHandler):
    def __init__(self):
        super().__init__(b'BKOR')

    def load(self, f: Reader, gs: GameState):
        for index, length in self.iterate_array(f, self.CH_ARRAY):
            f.read(length)


class OrderHandler(ChunkHandler):
    def __init__(self):
        super().__init__(b'ORDR')

    def load(self, f: Reader, gs: GameState):
        for index, length in self.iterate_array(f, self.CH_ARRAY):
            f.read(length)


class OrderListHandler(ChunkHandler):
    def __init__(self):
        super().__init__(b'ORDL')

    def load(self, f: Reader, gs: GameState):
        for index, length in self.iterate_array(f, self.CH_ARRAY):
            f.read(length)
