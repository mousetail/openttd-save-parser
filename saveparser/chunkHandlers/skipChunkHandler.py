from .chunkHandler import ChunkHandler
from saveparser.compression import Reader
from saveparser.gameState import GameState


class SkipChunkHandler(ChunkHandler):
    def __init__(self, name):
        super().__init__(name)

    def load(self, f: Reader, gs: GameState):
        for (number, length) in self.iterate_array(f, self.CH_ARRAY):
            f.read(length)