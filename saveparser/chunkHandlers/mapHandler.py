from saveparser.gameState.mapState import MapState
from . import chunkHandler


class MapSizeHandler(chunkHandler.ChunkHandler):
    def __init__(self):
        super().__init__(b"MAPS")

    def load(self, f, gs):
        gs.mapState.setSize((f.read_int(), f.read_int()))
        print("map size: " + str(gs.mapState.size))


class MapDatHandler(chunkHandler.ChunkHandler):
    MAP_SL_BUF_SIZE = 4096

    def __init__(self, key, index, bsize=1):
        super().__init__(key)
        self.index = index
        self.bsize = bsize

    def load(self, f, gs):
        size = gs.mapState.getTotalSize()
        i = 0
        while i != size:
            for j in range(self.MAP_SL_BUF_SIZE):
                self.handle(i,
                            f.read_ubyte() if self.bsize == 1 else f.read_ushort(), gs.mapState)
                # todo: actually parse this byte
                i += 1
                # print(i, k[:100])

    def handle(self, index, v, mp):
        tile = mp.get_index(index)
        self.handle_tile(tile, v)
        mp.set_index(index, tile)

    def handle_tile(self, tile, value):
        tile.data[self.index] = value


class MapTypeHandler(MapDatHandler):
    def __init__(self):
        super().__init__(b"MAPT", 0)

    def handle_tile(self, tile, v):
        tile.type = v >> 4


class MapHeightHandler(MapDatHandler):
    def __init__(self):
        super().__init__(b"MAPH", 1)

    def handle_tile(self, tile, value):
        tile.height = value


class MapOwnerHandler(MapDatHandler):
    def __init__(self):
        super().__init__(b"MAPO", 2)

    def handle_tile(self, tile, value):
        if (tile.type in (
            MapState.GROUND,
            MapState.RAIL,
            MapState.ROAD,
            MapState.TREE,
            MapState.STATION,
            MapState.WATER,
            MapState.BRIDGE,
            MapState.OBJECT,
        )):
            tile.owner = value & 0x1F
            if tile.owner > 0x0E:
                tile.owner = None
        else:
            tile.owner = None


class MapGeneral2Handler(MapDatHandler):
    def __init__(self):
        super().__init__(b"MAP2", 3, 2)


class MapGeneral3Handler(MapDatHandler):
    def __init__(self):
        super().__init__(b"M3LO", 5)


class MapGeneral4Handler(MapDatHandler):
    def __init__(self):
        super().__init__(b"M3HI", 6)


class MapGeneral5Handler(MapDatHandler):
    def __init__(self):
        super().__init__(b"MAP5", 7)


# These are the "Extended" map bits
class MapGeneral6Handler(MapDatHandler):
    def __init__(self):
        super().__init__(b"MAPE", 8)


class MapGeneral7Handler(MapDatHandler):
    def __init__(self):
        super().__init__(b"MAP7", 9)
