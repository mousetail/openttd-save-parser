from .chunkHandler import ChunkHandler
from saveparser.compression import Reader
from saveparser.gameState import GameState


class NewGRFHandler(ChunkHandler):
    def __init__(self):
        super().__init__(b'NGRF')

    def load(self, f: Reader, gs: GameState):
        for index, length in self.iterate_array(f, self.CH_ARRAY):
            position = f.tell()

            filename = f.read_string()
            if (filename):
                print("Loaded newgrf: "+filename.decode('utf-8'))

            f.seek(position + length)
