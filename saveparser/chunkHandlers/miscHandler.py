from . import chunkHandler
from saveparser.gameState.dateState import DateState
from saveparser.gameState import GameState
from saveparser.compression import Reader
from saveparser.gameState.viewState import ViewState


## SAVEGAME VERSION is 196

class DateHandler(chunkHandler.ChunkHandler):
    def __init__(self):
        super().__init__(b"DATE")

    def load(self, f: Reader, gs: GameState):
        date = f.read_int()
        date_fract = f.read_ushort()
        tick_counter = f.read_ushort()

        # cargo_age_counter = f.read_ubyte()
        cargo_age_counter = 0

        cur_tileloop_tile = f.read_uint()
        disaster_delay = f.read_short()

        random_state = [
            f.read_uint(),
            f.read_uint()
        ]

        cur_company_tick_index = f.read_ubyte()
        next_competitor_start = f.read_uint()
        trees_tick_ctr = f.read_ubyte()
        pause_mode = f.read_ubyte()
        # assert pause_mode == 0 or pause_mode == 1, pause_mode

        gs.date = DateState(
            date,
            date_fract,
            tick_counter,
            cargo_age_counter,
            cur_tileloop_tile,
            disaster_delay,
            random_state,
            cur_company_tick_index,
            next_competitor_start,
            trees_tick_ctr,
            pause_mode
        )

        print("Date: " + str(gs.date))


class ViewHandler(chunkHandler.ChunkHandler):
    def __init__(self):
        super().__init__(b'VIEW')

    def load(self, f: Reader, gs: GameState):
        gs.view = ViewState(
            f.read_int(),
            f.read_int(),
            f.read_ubyte()
        )


class CheatsHandler(chunkHandler.ChunkHandler):
    def __init__(self):
        super().__init__(b'CHTS')

    # These are the possible cheats as of version: (see cheat_sl.cpp)
    # (01) Cheat magic_bulldozer;  ///< dynamite industries, objects
    # (02) Cheat switch_company;   ///< change to another company
    # (03) Cheat money;            ///< get rich or poor
    # (04) Cheat crossing_tunnels; ///< allow tunnels that cross each other
    # (05) Cheat dummy1;           ///< empty cheat (build while in pause mode)
    # (06) Cheat no_jetcrash;      ///< no jet will crash on small airports anymore
    # (07)Cheat dummy2;           ///< empty cheat (change the climate of the map)
    # (08) Cheat change_date;      ///< changes date ingame
    # (09) Cheat setup_prod;       ///< setup raw-material production in game
    # (10) Cheat dummy3;           ///< empty cheat (enable running el-engines on normal rail)
    # (11) Cheat edit_max_hl;      ///< edit the maximum heightlevel; this is a cheat because of the fact that it needs to reset NewGRF game state and doing so as a simple configuration breaks the expectation of many

    num_cheats = 11

    def load(self, f: Reader, gs: GameState):
        for i in range(self.num_cheats):
            # Ignore the cheats, we don't need this information
            f.read_short()
