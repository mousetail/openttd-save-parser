import collections

from .chunkHandler import ChunkHandler
from saveparser.compression import Reader
from saveparser.gameState import GameState
from saveparser.gameState.companyState import Company

Economy = collections.namedtuple(
    "Economy",
    (
        "income",
        "expenses",
        "company_value",
        "delivered_cargo",
        "performance_history"
    )
)


class CompanyHandler(ChunkHandler):
    def __init__(self):
        super().__init__(b'PLYR')

    def load_economy(self, f: Reader) -> Economy:
        income = f.read_long()
        expenses = f.read_long()
        company_value = f.read_long()
        delivered_cargo = [f.read_int() for i in range(32)]
        performance_history = f.read_int()
        return Economy(
            income=income,
            expenses=expenses,
            company_value=company_value,
            delivered_cargo=delivered_cargo,
            performance_history=performance_history
        )

    def load(self, f: Reader, gs: GameState):
        for index, length in self.iterate_array(f, self.CH_ARRAY):
            position = f.tell()

            name_1 = f.read_uint()
            name_2 = f.read_short()
            name = f.read_string()
            president_name_1 = f.read_short()
            president_name_2 = f.read_uint()
            president_name = f.read_string()

            face = f.read_uint()
            money = f.read_long()
            loan = f.read_long()

            color = f.read_ubyte()
            money_fraction = f.read_ubyte()
            block_preview = f.read_ubyte()

            location_of_hq = f.read_uint()
            last_build_coordinate = f.read_uint()
            inaugurated_year = f.read_int()

            share_owners = [f.read_ubyte() for i in range(4)]
            num_valid_start_ent = f.read_ubyte()

            # Bankruptcy
            # 1 months, 2 asked, 2 timeout, 8 value
            bankruptcy_months = f.read_ubyte()
            bankruptcy_asked = f.read_ushort()
            bankrupt_timeout = f.read_ushort()
            bankrupt_value = f.read_ulong()

            yearly_expenses = [
                f.read_ulong()
                for i in range(3 * 13)
            ]

            is_ai = f.read_byte()
            assert is_ai in (0, 1)
            is_ai = bool(is_ai)

            terraform_limit = f.read_uint()
            clear_limit = f.read_uint()
            tree_limit = f.read_uint()

            # Skip company settings
            engine_renew_list = f.read_int()
            engine_renew = f.read_ubyte()
            assert engine_renew in (0, 1)
            engine_renew_months = f.read_ushort()
            engine_renew_money = f.read_uint()

            engine_renew_keep_length = f.read_ubyte()
            assert (engine_renew_keep_length in (0, 1))

            f.read(1 + 2 + 2 + 2 + 2)  # Default vehicle settings

            current_economy = self.load_economy(f)

            old_economy = [
                self.load_economy(f)
                for i in range(num_valid_start_ent)
            ]

            company = Company(
                index=index,
                company_name=name,
                president_name=president_name,
                money=money,
                loan=loan,
                is_ai=is_ai

            )
            gs.companies.append(company)

            f.seek(position + length)
