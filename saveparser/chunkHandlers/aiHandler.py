from saveparser.compression import Reader
from saveparser.gameState import GameState
from . import chunkHandler


class AIHandler(chunkHandler.ChunkHandler):
    def __init__(self):
        super().__init__(b'AIPL')

    def load(self, f: Reader, gs: GameState):
        for index, length in self.iterate_array(f, self.CH_ARRAY):
            pos = f.tell()

            ai_name = f.read_string()
            company = None
            for c in gs.companies:
                if c.index == index:
                    company = c

            if company:
                company.ai_name = ai_name

            f.seek(pos + length)
