from saveparser.chunkHandlers.chunkHandler import ChunkHandler
from saveparser.compression import Reader
from saveparser.gameState import GameState


class DepotHandler(ChunkHandler):
    def __init__(self):
        super().__init__(b"DEPT")

    def load(self, f: Reader, gs: GameState):
        for index, length in self.iterate_array(f, self.CH_ARRAY):
            f.read(length)
            gs.nrof_depots += 1
