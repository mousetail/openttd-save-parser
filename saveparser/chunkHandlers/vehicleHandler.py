import struct

from saveparser.compression import Reader
from saveparser.gameState import GameState
from saveparser.gameState.vehicleState import Vehicle
from .chunkHandler import ChunkHandler
from ..gameState import dateState


class VehicleHandler(ChunkHandler):
    OT_BEGIN = 0
    OT_NOTHING = 0
    OT_GOTO_STATION = 1
    OT_GOTO_DEPOT = 2
    OT_LOADING = 3
    OT_LEAVE_STATION = 4
    OT_DUMMY = 5
    OT_GOTO_WAYPOINT = 6
    OT_CONDITIONAL = 7
    OT_IMPLICIT = 8
    OT_END = 9

    VS_HIDDEN = 0x01,  # / < Vehicle is not visible.
    VS_STOPPED = 0x02,  # / < Vehicle is stopped
    VS_UNCLICKABLE = 0x04,  # / < Vehicle is not clickable
    VS_DEFPAL = 0x08,  # // / < Use default palet
    VS_TRAIN_SLOWING = 0x10,  # / < Train is slowing
    VS_SHADOW = 0x20,  # // / < Vehicle is a
    VS_AIRCRAFT_BROKEN = 0x40,  # // / < Aircraft is broken
    VS_CRASHED = 0x80,  # // / < Vehicle is crashed.

    EXTRA_PROCESSING = False

    min_values = {}

    ## 99 = 0x63 = 01100011
    ## LoadType = GB(4,3) = 0b110 =

    def __init__(self):
        super().__init__(b'VEHS')

    def load_vehicle_desciprion(self, f: Reader, vehicle_type: int, index: int, gs: GameState):
        subtype = f.read_ubyte()
        next = f.read_int()  # 32 bit int since version 69
        name = f.read_string()

        # if (len(name) > 2):
        #    print("vehicle name: " + str(index) + ": " + str(name) + " Type: " + str(vehicle_type) + " Subtype: " + str(
        #        subtype))
        # else:
        #    print("Vehicle Name: " + str(index) + " Type: " + str(vehicle_type))

        unitnumber = f.read_ushort()
        owner = f.read_ubyte()
        # print("\tOwner: {} Unit: {}".format(owner, unitnumber))
        if (unitnumber == 0):
            # It seems like the best way to detect shadow and other invalid vehicles is the lack of a proper
            # Unit number.
            return
        tile = f.read_uint()
        assert tile < gs.mapState.getTotalSize()
        dest_tile = f.read_uint()
        assert dest_tile < gs.mapState.getTotalSize()
        x_pos = f.read_uint() // gs.mapState.TILE_SIZE
        y_pos = f.read_uint() // gs.mapState.TILE_SIZE
        z_pos = f.read_uint()

        assert x_pos < gs.mapState.size[0], x_pos
        assert y_pos < gs.mapState.size[1], y_pos
        # print("\tPosition: {} {} {}".format(x_pos, y_pos, z_pos))
        # ("\tTile: {} Dest Tile: {}".format(gs.mapState.get_index(tile),
        #                                         gs.mapState.get_index(dest_tile)))

        if tile != 0:
            tile_obj = gs.mapState.get_index(tile)
            # assert tile_obj.x == x_pos, "{} != {} tile: {}".format(tile_obj.x, x_pos, tile)
            # assert tile_obj.y == y_pos, "{} != {} tile: {}".format(tile_obj.y, y_pos, tile)

        direction = f.read_byte()
        assert direction <= 8, direction
        # print("\tDirection: ", direction)

        spritenum = f.read_ubyte()
        engine_type = f.read_ushort()
        # print("\tengine_type: ", engine_type)
        # print("\tSprite Num: ", spritenum)
        cur_speed = f.read_ushort()
        subspeed = f.read_ubyte()

        acceleration = f.read_ubyte()
        progress = f.read_ubyte()

        vehstatus = f.read_ubyte()

        # print("\tstatus: ", hex(vehstatus))

        last_station_visited = f.read_ushort()
        last_loading_station = f.read_ushort()
        # assert last_loading_station == last_station_visited, "{} {}".format(last_loading_station, last_station_visited)

        cargo_type = f.read_ubyte()
        cargo_subtype = f.read_ubyte()

        cargo_cap = f.read_short()
        refit_cap = f.read_short()
        # print("\tCargo Capacity: ", cargo_cap, "type: ", cargo_type, "refit cap", str(refit_cap))

        cargo_packets = f.read_list(200)
        cargo_actions = tuple(f.read_uint() for i in range(4))
        cargo_age = f.read_ushort()

        day_counter = f.read_ubyte()
        tick_counter = f.read_ubyte()
        running_ticks = f.read_ubyte()

        cur_implicit_order_index = f.read_ubyte()
        cur_real_order_index = f.read_ubyte()

        # assert cur_real_order_index <= cur_implicit_order_index or cur_implicit_order_index == 0, str(
        #    cur_real_order_index) + ", " + str(
        #    cur_implicit_order_index)

        # assert (0 <= cur_real_order_index <= 4), cur_real_order_index
        # assert (0 <= cur_implicit_order_index <= 4), cur_implicit_order_index

        def gb(x, s, n):
            return (x >> s) & ((1 << n) - 1)

        cur_order_type = gb(f.read_ubyte(), 0, 4)
        cur_order_flags = f.read_ubyte()
        cur_order_dest = f.read_ushort()

        load_flags = gb(cur_order_flags, 4, 3)
        unload_flags = gb(cur_order_flags, 0, 3)
        non_stop_flags = gb(cur_order_flags, 6, 2)

        assert cur_order_dest <= gs.mapState.getTotalSize(), cur_order_dest

        assert cur_order_type in (
            self.OT_BEGIN,
            self.OT_NOTHING,
            self.OT_GOTO_STATION,
            self.OT_GOTO_DEPOT,
            self.OT_LOADING,
            self.OT_LEAVE_STATION,
            self.OT_DUMMY,
            self.OT_GOTO_WAYPOINT,
            self.OT_CONDITIONAL,
            self.OT_IMPLICIT,
            self.OT_END
        ), cur_order_type

        # if cur_order_type != self.OT_GOTO_STATION:
        #    print("\tSuspicious order type: ", cur_order_type, "flags: ", load_flags, unload_flags, non_stop_flags)

        cur_order_refit_cargo = f.read_ubyte()
        # assert cur_order_refit is 0 or cur_order_refit == cargo_type, "{} {}".format(
        #    cur_order_refit,
        #    cargo_type)

        cur_order_wait = f.read_ushort()
        cur_order_travel_time = f.read_ushort()
        cur_order_max_speed = f.read_ushort()
        cur_order_timetable_start = f.read_int()

        orders = f.read_uint()

        age = f.read_int()
        # print("\tVehicle age: ", age)
        max_age = f.read_int()
        # print("\tVehicle Max Age: ", max_age)
        date_of_last_service = f.read_int()
        # assert age >= 0 and max_age >= 0, "age {}, max_age: {}".format(age, max_age)
        service_interval = f.read_ushort()
        reliability = f.read_ushort() * 101 >> 16
        reliability_speed_decrease = f.read_ushort()
        breakdown_control = f.read_ubyte()
        breakdown_delay = f.read_ubyte()
        breakdowns_since_last_service = f.read_ubyte()
        breakdown_chance = f.read_ubyte() * 101 >> 16
        # print("\tLast Service at: ", dateState.DateState.int_to_date(date_of_last_service),
        #      "breakdowns:", breakdowns_since_last_service)
        # assert breakdown_chance < 100, breakdown_chance
        # print("\tBreakdown chance: ", breakdown_chance," Reliability: ",reliability,'%')

        build_year = f.read_uint()
        # assert (1900 <= build_year <=2100), build_year
        # print("\tBuild Year: ", build_year)

        load_unload_ticks = f.read_ushort()
        cargo_paid_for = f.read_ushort()
        vehicle_flags = f.read_ushort()
        # print('\tFlags:'+bin(vehicle_flags))
        profit_this_year = f.read_long()
        profit_last_year = f.read_long()
        value = f.read_long()
        profit_this_year >>= 8
        profit_last_year >>= 8

        # profit_this_year = f.read_long()
        # profit_last_year = f.read_long()
        # value = f.read_long()

        # print("\tProfit: {: 9,} Last: {: 9,} Value: {: 9,}".format(profit_this_year, profit_last_year,
        #                                                                       value))

        vehicle = Vehicle(
            name,
            vehicle_type,
            (x_pos, y_pos, z_pos),
            engine_type,
            0,
            owner,
            value
        )

        return vehicle
        # cargo

    ## VEHICLE TYPES:
    ## 0 - Train
    ## 1 - Road
    ## 2 - Ship
    ## 3 - Aircraft
    ## 4 - Company End - Same as effect
    ## 4 - Effect
    ## 5 - Disaster
    ## 6 - End

    def load(self, f: Reader, gs: GameState):
        for index, length in self.iterate_array(f, self.CH_SPARSE_ARRAY):
            start = f.tell()
            vehicle_type = f.read_ubyte()
            assert isinstance(vehicle_type, int)
            # print("vehicle index: {}, length: {}, type: {}".format(
            #    index, length, type))
            assert 0 <= vehicle_type <= 6, vehicle_type

            if 0 <= vehicle_type <= 3:
                description = self.load_vehicle_desciprion(f, vehicle_type, index, gs)
                if description is not None:
                    gs.vehicles.append(description)
            f.seek(start + length)
            assert (f.tell() == start + length)
