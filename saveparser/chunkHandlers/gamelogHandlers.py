from saveparser.gameState import gameLogState

from . import chunkHandler, grfHandler


class GamelogHandler(chunkHandler.ChunkHandler):
    NETWORK_REVISION_LENGTH = 15

    def __init__(self):
        super(GamelogHandler, self).__init__(b"GLOG")

    def load(self, f, gs):
        num = 0
        byte = f.read_ubyte()
        while byte != 255:
            # num += 1
            # print (byte, end="\n" if num%30 == 0 else "")
            # continue
            action = gameLogState.LoggedAction([], byte, None)
            action.time = f.read_short()

            byte2 = f.read_ubyte()
            while byte2 != 255:
                if byte2 == 0:
                    b = self.parseModeDesc(f, byte2)
                elif byte2 == 1:
                    b = self.parseRevisionDesc(f, byte2)
                elif byte2 == 2:
                    b = self.parseOldverDesc(f, byte2)
                elif byte2 == 3:
                    b = self.parseSettingDesc(f, byte2)
                elif byte2 == 4:
                    b = self.parseGrfaddDesc(f, byte2)
                elif byte2 == 5:
                    b = self.parseGrfremDesc(f, byte2)
                elif byte2 == 6:
                    b = self.parseGrfcompatDesc(f, byte2)
                elif byte2 == 7:
                    b = self.parseGrfparamDesc(f, byte2)
                elif byte2 == 8:
                    b = self.parseGrfmoveDesc(f, byte2)
                elif byte2 == 9:
                    b = self.parseGrfbugDesc(f, byte2)
                elif byte2 == 10:
                    b = self.parseEmergencyDesc(f, byte2)
                else:
                    raise ValueError("invalid byte 2: " + str(byte2))
                action.changes.append(b)
                # todo: load stuff
                byte2 = f.read_ubyte()
            gs.gameLogState.actions.append(action)
            byte = f.read_ubyte()

    def parseModeDesc(self, f, glc_type):
        return gameLogState.ModeChange(glc_type, f.read(1),
                                       f.read(1))

    def parseRevisionDesc(self, f, glc_type):
        return gameLogState.RevisionChange(
            glc_type,
            f.read(self.NETWORK_REVISION_LENGTH),
            f.read_uint(),
            f.read_ushort(),
            f.read_ubyte()
        )

    def parseOldverDesc(self, f, glc_type):
        return gameLogState.SettingChange(
            glc_type,
            f.read_uint(),
            f.read_uint(),
        )

    def parseGrfaddDesc(self, f, glc_type):
        return gameLogState.GrfAddChange(
            glc_type,
            grfHandler.GrfHandler.load_grf_id(f)
        )

    def parseGrfremDesc(self, f, glc_type):
        return gameLogState.GrfRemChange(
            glc_type,
            grfHandler.GrfHandler.load_grf_id(f)
        )

    def parseGrfcompatDesc(self, f, glc_type):
        return gameLogState.GrfCompatChange(
            glc_type,
            grfHandler.GrfHandler.load_grf_id(f)
        )

    def parseGrfparamDesc(self, f, glc_type):
        return gameLogState.GrfParamChange(
            glc_type,
            grfHandler.GrfHandler.load_grf_id(f)
        )

    def parseGrfmoveDesc(self, f, glc_type):
        return gameLogState.GrfMoveChange(
            glc_type,
            f.read(4),
            f.read(4)
        )

    def parseSettingDesc(self, f, glc_type):
        return gameLogState.SettingChange(
            glc_type,
            f.read_string(),
            f.read(4),
            f.read(4)
        )

    def parseGrfbugDesc(self, f, glc_type):
        return gameLogState.GrfBugChange(
            glc_type,
            f.read_ulong(),
            f.read_uint(),
            f.read_ubyte()
        )

    def parseEmergencyDesc(self, f, glc_type):
        return gameLogState.EmergencyState(
            glc_type
        )
