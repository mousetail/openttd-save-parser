import struct
import typing

from saveparser.chunkHandlers.skipChunkHandler import SkipChunkHandler
from .chunkHandlers.chunkHandler import ChunkHandler


class ChunkLoader(object):
    CH_ARRAY = 1
    CH_SPARSE_ARRAY = 2
    CH_RIFF = 0

    def __init__(self, chunk_handlers, ignore_chunk_handlers, gameState):
        self.chunk_handlers = chunk_handlers
        self.ignore_chunk_handlers = ignore_chunk_handlers
        self.gameState = gameState

    def load_chunks(self, f):
        while self.load_chunk(f):
            pass

    def load_sparse_array(self, f):
        pass

    def load_chunk(self, f):
        d = f.read(4)
        if len(d) < 4:
            return False
        ch_id = b''.join(struct.unpack('!cccc', d))
        if ch_id == b'\x00\x00\x00\x00':
            return False

        handler = self.find_chunk_handler(ch_id)
        if handler is None:
            raise ValueError("Unrecogized chunk type: " + str(ch_id))

        tp = ord(f.read(1))
        if tp == self.CH_ARRAY:
            if isinstance(handler, bytes):
                SkipChunkHandler(handler).load(f, self.gameState)
            else:
                handler.load(f, self.gameState)
        elif tp == self.CH_SPARSE_ARRAY:
            # Sparse arrays do not include length
            handler.load(f, self.gameState)
        elif tp & 0xf == self.CH_RIFF:
            length = (ord(f.read(1)) << 16) | (tp >> 4) << 24
            length += struct.unpack("!H", f.read(2))[0]
            if isinstance(handler, bytes):
                f.read(length)
            else:
                handler.load(f, self.gameState)
        else:
            raise ValueError("invalid value")
        return True

    def find_chunk_handler(self, ch_id) -> typing.Union[ChunkHandler, None]:
        # define FOR_ALL_CHUNK_HANDLERS(ch) \
        # for (const ChunkHandler * const * chsc = _chunk_handlers;
        #  * chsc != NULL; chsc++) \
        #        for (const ChunkHandler * ch = * chsc;
        # ch != NULL;
        # ch = (ch->flags & CH_LAST) ? NULL: ch + 1)
        for handler in self.chunk_handlers:
            # print (handler.id, ch_id)
            assert isinstance(handler.id, bytes)
            if handler.id == ch_id:
                return handler
        for handler in self.ignore_chunk_handlers:
            if handler == ch_id:
                return handler
        return None
