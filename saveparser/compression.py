import lzma
import struct
import abc


def detect_format(f):
    f.seek(0)
    fmt = f.read(4)
    version = struct.unpack('!I', f.read(4))[0]  # skip the second 4 bytes of header

    if fmt == b"OTTD":  # LZO compression
        pass
    elif fmt == b"OTTN":  # No compression
        pass
    elif fmt == b"OTTZ":  # ZLIB compression
        pass
    elif fmt == b"OTTX":  # LZMA compression
        return LZMAReader(f)
    else:
        raise ValueError("Unkown compression type: " +
                         fmt.decode("utf-8"))


class Reader(object, metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def read(self, length):
        raise NotImplementedError()

    def read_int(self) -> int:
        return struct.unpack("!i", self.read(4))[0]

    def read_uint(self) -> int:
        return struct.unpack("!I", self.read(4))[0]

    def read_short(self) -> int:
        return struct.unpack("!h", self.read(2))[0]

    def read_ushort(self) -> int:
        return struct.unpack("!H", self.read(2))[0]

    def read_byte(self) -> int:
        return struct.unpack("!b", self.read(1))[0]

    def read_long(self) -> int:
        return struct.unpack('!q', self.read(8))[0]

    def read_ulong(self) -> int:
        return struct.unpack('!Q', self.read(8))[0]

    def read_ubyte(self) -> int:
        return ord(self.read(1))

    def read_gamma(self):
        b = self.read_ubyte()
        if b & 0x80:
            b &= ~0x80
            if b & 0x40:
                b &= ~0x40
                if b & 0x20:
                    b &= ~0x20
                    if b & 0x10:
                        b &= ~0x10
                        if b & 0x08:
                            raise ValueError("Unsupported Gamma Format")
                        b = self.read_ubyte()  # 32 bits
                    b = (b << 8) | self.read_ubyte()  # 32 bits
                b = (b << 8) | self.read_ubyte()  # 24 bits
            b = (b << 8) | self.read_ubyte()  # 16 bits
        return b  # 8 bits

    def read_string(self) -> bytes:
        length = self.read_gamma()
        if length == 0:
            return b''
        else:
            bf = self.read(length)
        return bf

    def seek(self, position: int):
        return NotImplemented

    def tell(self) -> int:
        raise NotImplementedError()

    def read_list(self, limit=None):
        length = self.read_uint()
        if limit is not None and length > limit:
            raise ValueError("Reached Length Limit: "+str(length))
        return tuple(self.read_int() for i in range(length))


class LZMAReader(Reader):
    def __init__(self, nxt):
        self.next = nxt
        self.reader = lzma.LZMADecompressor()
        self.file_position = 0
        pass

    def seek(self, position):
        """Only supports seeking forwards"""
        if position < self.file_position:
            raise ValueError(
                "Cannot seek backwards from {} to {}".format(
                    self.file_position, position))
        self.read(position - self.tell())

    def tell(self):
        return self.file_position

    def _read(self, limit):
        if self.reader.needs_input:
            return self.reader.decompress(self.next.read(1024),
                                          limit)  # read a kilobyte
        else:
            buffer = self.reader.decompress(b"", limit)
            while len(buffer) < limit:
                t = self.next.read(1024)
                if len(t) == 0:
                    return buffer
                buffer += self.reader.decompress(
                    t, limit - len(buffer))
            return buffer

    def read(self, limit):
        result = self._read(limit)
        self.file_position += len(result)
        return result
