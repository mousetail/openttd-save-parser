import sys
import os
import uuid
import json, sentry_sdk
from collections import namedtuple

sys.path.append('')
from webapp import db

from saveparser.gameState import vehicleState
from saveparser.gameState import companyState
from saveparser import saveLoader
from saveparser.metrics import money

metrics = [
    money
]


def calculate_elo(score, elo_a, elo_b, weight=32):
    expected_score = 1 / (1 + 10 ** ((elo_b - elo_a) / 400))
    return elo_a + weight * (score - expected_score)


def get_ai_elo(cursor, ai_id: int, preset: str, phase: int, metric: str):
    cursor.execute(
        """
        SELECT elo
        FROM elos
        WHERE ai=%s
            AND preset=%s
            AND phase=%s
            AND metric=%s
        """,
        (ai_id, preset, phase, metric)
    )

    res = cursor.fetchone()
    if res:
        return res[0]
    else:
        return 0


def fetch_ai_data(cursor, ai_id: int, preset: str, phase: int):
    assert isinstance(ai_id, int)
    assert isinstance(preset, str)
    assert isinstance(phase, int)

    cursor.execute(
        """
            SELECT ai.id, ai.name
            FROM ai
            WHERE
                ai.id=(%s)
            """,
        (ai_id,)
    )
    result = cursor.fetchone()
    return result


def count_map_control(gs, *companies):
    output = [{'total': 0, 'station': 0, 'rail': 0, 'road': 0} for i in companies]
    for pos in range(gs.mapState.getTotalSize()):
        tile = gs.mapState.get_index(pos)
        for index, company in enumerate(companies):
            if tile.owner == company.index and tile.type != gs.mapState.VOID:
                output[index]['total'] += 1
                if tile.type == gs.mapState.STATION:
                    output[index]['station'] += 1
                elif tile.type == gs.mapState.RAIL:
                    output[index]['rail'] += 1
                elif tile.type == gs.mapState.ROAD:
                    output[index]['road'] += 1
                break

    return output


def calculate_company_value(gs, *companies):
    company_ids = [i.index for i in companies]
    value = {i.index: 0 for i in companies}

    for vehicle in gs.vehicles:
        if vehicle.owner in company_ids:
            value[vehicle.owner] += vehicle.value

    return {index: value[company.index] for index, company in enumerate(companies)}


def count_vehicles(gs, *companies):
    company_ids = [i.index for i in companies]
    result = {i: {"road": 0, "train": 0, "ship": 0, "plane": 0} for i in company_ids}

    type_map = {
        vehicleState.Vehicle.VEH_ROAD: 'road',
        vehicleState.Vehicle.VEH_TRAIN: 'train',
        vehicleState.Vehicle.VEH_PLANE: 'plane',
        vehicleState.Vehicle.VEH_SHIP: 'ship'
    }

    for vehicle in gs.vehicles:
        if vehicle.owner in company_ids and vehicle.vehicle_type in type_map:
            result[vehicle.owner][type_map[vehicle.vehicle_type]] += 1
        else:
            print("Invalid vehicle, owner: {}, type: {}".format(vehicle.owner, vehicle.vehicle_type))
    return [result[company.index] for company in companies]


def update_ai(cursor, ai_id, preset, phase, metric, new_elo):
    cursor.execute("""
    SELECT preset FROM elos
    WHERE ai=%s
        AND preset=%s
        AND phase=%s
        AND metric=%s
    """, (ai_id, preset, phase, metric))

    if cursor.fetchone() is not None:
        cursor.execute(
            """
            UPDATE elos
            SET elo=(%s)
            WHERE ai=(%s)
                AND preset=(%s)
                AND phase=(%s)
                AND metric=(%s)""",
            (new_elo, ai_id, preset, phase, metric)
        )
    else:
        cursor.execute("INSERT INTO elos(ai, preset, phase, metric, elo) VALUES "
                       "(%s, %s, %s, %s, %s)",
                       (ai_id, preset, phase, metric, new_elo)
                       )


def insert_match_elos(cursor, match_id, preset, metric, ai1_id, ai2_id, elo1_delta, elo2_delta, value1, value2):
    cursor.execute(
        """
    INSERT INTO metrics(match, metric, preset, ai_1, ai_2, value_1, value_2, elo_1, elo_2)
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
    """,
        (match_id, metric, preset, ai1_id, ai2_id, value1, value2, elo1_delta, elo2_delta)
    )


def analize(save, image_location: str, pending_match_id: int, client_preset: str, phase: int,
            match_id: int) -> None:
    gamestate = saveLoader.decode(save)
    image_id = str(uuid.uuid4()) + ".png"
    image_url = "images/" + image_id
    image_path = os.path.join(image_location, image_id)
    saveLoader.generate_image(gamestate, image_path)
    companies = gamestate.companies
    assert len(companies) <= 2

    save_path = os.path.join('saves', os.path.split(save)[-1])

    dbhandler = db.DBHandler()
    cursor = dbhandler.get_cursor()

    companies_info = [
        {},
        {}
    ]

    with cursor:
        match = cursor.execute("""
    SELECT opponent_1,
        opponent_2,
        preset
    FROM pending_matches
    WHERE id=(%s)
    """, (pending_match_id,))

        companies_info[0]["ai_id"], companies_info[1]["ai_id"], preset = cursor.fetchone()
        assert preset == client_preset

        ai1data = fetch_ai_data(cursor, companies_info[0]["ai_id"], preset, phase)
        ai2data = fetch_ai_data(cursor, companies_info[1]["ai_id"], preset, phase)

        if len(companies) == 1:
            if (companies[0].ai_name.decode('utf-8').lower()) == ai1data[1].lower():
                other_company_name = ai2data[1]
            else:
                other_company_name = ai1data[1]

            companies.append(
                companyState.Company(
                    -1,
                    b"",
                    b"",
                    -999999,
                    0,
                    True,
                    ai_name=other_company_name.encode('utf-8')
                )
            )

        if companies[0].ai_name.decode('utf-8').lower() == ai2data[1].lower():
            companies = companies[1], companies[0]

        if (companies[0].ai_name.decode('utf-8').lower() == ai1data[1].lower() and
                companies[1].ai_name.decode('utf-8').lower() == ai2data[1].lower()):
            print("Verified AI's")
        else:
            print("AI Should be: {0:02}{1: <25} and {2:02}{3: <25}".format(
                companies_info[0]["ai_id"], ai1data[1].lower(),
                companies_info[1]["ai_id"], ai2data[1].lower()))
            print("         Was:   {0: <25} and   {1: <25}".format(companies[0].ai_name.decode('utf-8').lower(),
                                                                   companies[1].ai_name.decode('utf-8').lower()))
            raise ValueError("Wrong AIs")

        companies_info[0]["value"], companies_info[1]["value"] = calculate_company_value(
            gamestate, companies[0],
            companies[1])

        companies_info[0]["money"] = companies[0].money - companies[0].loan + companies_info[0]["value"]
        companies_info[1]["money"] = companies[1].money - companies[1].loan + companies_info[1]["value"]

        companies_info[0]["vehicle_counts"], companies_info[1]["vehicle_counts"] = count_vehicles(
            gamestate, companies[0], companies[1])
        companies_info[0]["map_control"], companies_info[1]["map_control"] = count_map_control(
            gamestate, companies[0], companies[1])

        cursor.execute(
            """
        UPDATE matches
        SET preset=%s,
            phase=%s,
            image_name=%s,
            save_name=%s,
            opponent_0=%s,
            opponent_1=%s,
            money_0=%s,
            money_1=%s,
            company_value_1=%s,
            company_value_2=%s,
            ai_1_loan=%s,
            ai_2_loan=%s,
            ai_1_total_vehicles=%s,
            ai_2_total_vehicles=%s,
            ai_1_road_vehicles=%s,
            ai_2_road_vehicles=%s,
            ai_1_trains=%s,
            ai_2_trains=%s,
            ai_1_ships=%s,
            ai_2_ships=%s,
            ai_1_planes=%s,
            ai_2_planes=%s,
            ai_1_map_control=%s,
            ai_2_map_control=%s,
            map_control_stations_1=%s,
            map_control_stations_2=%s,
            map_control_road_1=%s,
            map_control_road_2=%s,
            map_control_rail_1=%s,
            map_control_rail_2=%s,
            pending_match_id=%s
        WHERE id=%s""",
            (
                preset,
                phase,
                image_url,
                save_path,
                companies_info[0]["ai_id"],
                companies_info[1]["ai_id"],
                companies_info[0]["money"],
                companies_info[1]["money"],
                companies_info[0]["value"],
                companies_info[1]["value"],
                companies[0].loan,
                companies[1].loan,
                sum(companies_info[0]["vehicle_counts"].values()),
                sum(companies_info[1]["vehicle_counts"].values()),
                companies_info[0]["vehicle_counts"]["road"],
                companies_info[1]["vehicle_counts"]["road"],
                companies_info[0]["vehicle_counts"]["train"],
                companies_info[1]["vehicle_counts"]["train"],
                companies_info[0]["vehicle_counts"]["ship"],
                companies_info[1]["vehicle_counts"]["ship"],
                companies_info[0]["vehicle_counts"]["plane"],
                companies_info[1]["vehicle_counts"]["plane"],
                companies_info[0]["map_control"]['total'],
                companies_info[1]["map_control"]["total"],
                companies_info[0]["map_control"]['station'],
                companies_info[1]["map_control"]["station"],
                companies_info[0]["map_control"]['road'],
                companies_info[1]["map_control"]["road"],
                companies_info[0]["map_control"]['rail'],
                companies_info[1]["map_control"]["rail"],
                pending_match_id,
                match_id
            ))


        for metric in metrics:
            winner, value1, value2 = metric.calculate_money(gamestate, *companies, *companies_info)

            elo1 = get_ai_elo(cursor, companies_info[0]["ai_id"], preset, phase, metric.name)
            elo2 = get_ai_elo(cursor, companies_info[1]["ai_id"], preset, phase, metric.name)
            elo1_overall = get_ai_elo(cursor, companies_info[0]["ai_id"], "overall", phase, metric.name)
            elo2_overall = get_ai_elo(cursor, companies_info[1]["ai_id"], "overall", phase, metric.name)

            ai1_elo_new = calculate_elo(1 - winner, elo1, elo2)
            ai2_elo_new = calculate_elo(winner, elo2, elo1)
            ai1_elo_overall_new = calculate_elo(1 - winner, elo1_overall, elo2_overall)
            ai2_elo_overall_new = calculate_elo(winner, elo2_overall, elo1_overall)

            update_ai(cursor, companies_info[0]["ai_id"], preset, phase, metric.name, ai1_elo_new)
            update_ai(cursor, companies_info[1]["ai_id"], preset, phase, metric.name, ai2_elo_new)
            # Update overall ranking
            update_ai(cursor, companies_info[0]["ai_id"], "overall", phase, metric.name, ai1_elo_overall_new)
            update_ai(cursor, companies_info[1]["ai_id"], "overall", phase, metric.name, ai2_elo_overall_new)

            insert_match_elos(
                cursor, match_id, preset, metric.name,
                companies_info[0]["ai_id"], companies_info[1]["ai_id"],
                ai1_elo_new - elo1, ai2_elo_new - elo2, value1,
                value2)
            insert_match_elos(
                cursor, match_id, "overall", metric.name,
                companies_info[0]["ai_id"], companies_info[1]["ai_id"],
                ai1_elo_overall_new - elo1_overall, ai2_elo_overall_new - elo2_overall,
                value1, value2)

        cursor.connection.commit()
        print("Done!")

        # RAn = RA + K * (SA - EA)
        # Ra = new ELO rating
        # RA = Current ELO rating
        # K Weighting factory
        # Result for player A [0 to 1]
        # Expected score for player A

        # Expected score = 1 / (1 + 10**(RB- RA)/400)


if __name__ == "__main__":
    if len(sys.argv) != 7:
        print("Usage: ")
        print("python3 analize.py [save] [image root folder] [pending_match_id] [preset] [phase] [match]")
        exit(1)

    with open('config.json') as f:
        config = json.load(f)
    sentry_sdk.init(
        config['sentry_url'])

    analize(str(sys.argv[1]),
            sys.argv[2],
            int(sys.argv[3]),
            str(sys.argv[4]),
            int(sys.argv[5]),
            int(sys.argv[6]))
