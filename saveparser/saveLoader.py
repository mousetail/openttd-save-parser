import sys

from saveparser import gameState, chunkHandlers, chunkLoader, compression
from PIL import Image


def decode(fname):
    gs = gameState.GameState()
    with open(fname, "rb") as f:
        wrap = compression.detect_format(f)
        ch_loader = chunkLoader.ChunkLoader(
            chunkHandlers.get_all_handlers(),
            chunkHandlers.get_ignored_handlers(),
            gs)
        ch_loader.load_chunks(wrap)
    return gs


def generate_image(gs, filename):
    WATER_COLOR = (0xe6, 0xf2, 0xfb)
    BG_COLOR = (255, 255, 255)
    INDUSTRY_COLOR = (0xf1, 0xf1, 0xf1)

    colors = (
        (255, 0, 0),
        (0, 255, 0),
        (0, 0, 255),
    )

    image = Image.new('RGB', gs.mapState.size)
    for pos in range(gs.mapState.getTotalSize()):
        tile = gs.mapState.get_index(pos)
        if tile.owner is None or tile.type == gs.mapState.VOID:
            if tile.type in (
                    gs.mapState.GROUND,
                    gs.mapState.VOID,
                    gs.mapState.TREE,
                    gs.mapState.OBJECT):
                color = BG_COLOR
            elif tile.type == gs.mapState.WATER or tile.type == gs.mapState.VOID:
                color = WATER_COLOR
            else:
                color = INDUSTRY_COLOR
        elif tile.owner is not None:
            color = colors[tile.owner % len(colors)]
            if tile.type == gs.mapState.STATION:
                color = tuple(i // 2 for i in color)

        image.putpixel((tile.x, tile.y),
                       color)
    with open(filename, "wb") as f:
        image.save(f, 'png')


if __name__ == "__main__":
    decode(sys.argv[1])
