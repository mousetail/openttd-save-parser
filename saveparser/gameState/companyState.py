from saveparser.genericObject import GenericObject


class Company(GenericObject):
    def __init__(self, index,
                 company_name,
                 president_name,
                 money,
                 loan,
                 is_ai,
                 ai_name=None,
                 ):
        self.index = index
        self.company_name = company_name
        self.president_name = president_name
        self.money = money
        self.loan = loan
        self.is_ai = is_ai
        self.ai_name = ai_name
