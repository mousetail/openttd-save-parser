from saveparser import genericObject


class LoggedAction(genericObject.GenericObject):
    def __init__(self, changes, change_type, time):
        self.changes = changes
        self.type = change_type
        self.time = time


class LoggedChange(genericObject.GenericObject):
    def __init__(self, glc_type):
        self.type = glc_type


class ModeChange(LoggedChange):
    def __init__(self, glc_type, mode, landscape):
        super().__init__(glc_type)
        self.mode = mode
        self.landscape = landscape

    def __str__(self):
        return self.__class__.__name__ + " mode: " + \
               str(self.mode) + " landscape: " + str(self.landscape)


class RevisionChange(LoggedChange):
    def __init__(self, glc_type, revision, newgrf,
                 silver, modified):
        super().__init__(glc_type)
        self.revision = revision
        self.newgrf = newgrf
        self.silver = silver
        self.modified = modified


class OldverChange(LoggedChange):
    def __init__(self, glc_type, o_type, version):
        super().__init__(glc_type)
        self.o_type = o_type
        self.version = version


class GrfChange(LoggedChange):
    def __init__(self, glc_type, grfid):
        super().__init__(glc_type)
        self.grfid = grfid


class GrfAddChange(GrfChange):
    pass


class GrfRemChange(GrfChange):
    pass


class GrfCompatChange(GrfChange):
    pass


class GrfParamChange(GrfChange):
    pass


class GrfMoveChange(GrfChange):
    def __init__(self, glc_type, grfid, offset):
        super().__init__(glc_type, grfid)
        self.offset = offset


class SettingChange(LoggedChange):
    def __init__(self, glc_type, param, old_value, new_value):
        super().__init__(glc_type)
        self.param = param
        self.old_value = old_value
        self.new_value = new_value


class GrfBugChange(LoggedChange):
    def __init__(self, glc_type, data, grfid, bug):
        super().__init__(glc_type)
        self.data = data
        self.grfid = grfid
        self.bug = bug


class EmergencyState(LoggedChange):
    pass


class GameLogState:
    def __init__(self):
        self.actions = []

    def add_action(self, action):
        self.actions.append(action)
