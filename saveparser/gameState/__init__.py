from . import gameLogState, mapState

class GameState(object):
    def __init__(self):
        self.gameLogState = gameLogState.GameLogState()
        self.mapState = mapState.MapState()
        self.date = None
        self.vehicles = []
        self.nrof_depots = 0
        self.companies = []
        pass