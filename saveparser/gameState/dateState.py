DAYS_IN_YEAR = 365

class DateState(object):
    def __init__(self, date,
                 date_fract,
                 tick_counter,
                 age_cargo_skip_counter,
                 cur_tileloop_tile,
                 disaster_delay,
                 random_state,
                 cur_company_tick_index,
                 next_competitor_start,
                 trees_tick_ctr,
                 pause_mode
                 ):
        self.date = date
        self.date_fract = date_fract
        self.tick_counter = tick_counter
        self.age_cargo_skip_counter = age_cargo_skip_counter,

    def date_to_ymd(self):
        self.int_to_date(self.date)

    @staticmethod
    def int_to_date(date_int):
        year = (400 * date_int) // (DAYS_IN_YEAR * 400 + 97)

        return (year)

    def __str__(self):
        return "DateState(" + str(self.date) + ")" + str(self.date_to_ymd())
