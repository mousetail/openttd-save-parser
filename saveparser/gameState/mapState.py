from saveparser import genericObject


class TileIndex(genericObject.GenericObject):

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return isinstance(other, Tile) and \
               self.x == other.x and self.y == other.y

    def next_tile(self, map):
        t2 = Tile(self.x + 1, self.y)
        if t2.x > map.size[0]:
            t2.x = 0
            t2.y += 1
        return t2


class Tile(TileIndex):
    def __init__(self, x, y, mptype, terrain):
        super().__init__(x, y)
        self.data = [0 for i in range(10)]
        self.height = None
        self.owner = None
        self.type = mptype


class MapState(genericObject.GenericObject):
    GROUND = 0x00
    RAIL = 0x01
    ROAD = 0x02
    BUILDING = 0x03
    TREE = 0x04
    STATION = 0x05
    WATER = 0x06
    VOID = 0x07
    INDUSTRY = 0x08
    BRIDGE = 0x09
    OBJECT = 0x0A


    TILE_SIZE = 16

    def __init__(self):
        self.size = [0, 0]
        self.dat = None

    def setSize(self, size):
        self.size = size
        self.dat = [[Tile(i, j, None, None) for i in range(size[1])]
                    for j in range(size[0])]

    def getTotalSize(self):
        return self.size[0] * self.size[1]

    def get_index(self, index) -> Tile:
        return self.dat[index % self.size[0]][index // self.size[0]]

    def set_index(self, index, value):
        self.dat[index % self.size[0]][index // self.size[0]] = value
