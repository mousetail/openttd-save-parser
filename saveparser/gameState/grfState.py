from saveparser import genericObject


class GrfID(genericObject.GenericObject):
    def __init__(self, grf_id, checksum):
        self.grf_id = grf_id
        self.checksum = checksum