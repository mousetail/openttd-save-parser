class Vehicle(object):
    VEH_TRAIN = 0
    VEH_ROAD = 1
    VEH_SHIP = 2
    VEH_PLANE = 3

    def __init__(self, name: bytes, vehicle_type: int, position: (int, int, int),
                 engine_type: int, cargo_type: int, owner: int, value: int):
        assert isinstance(vehicle_type, int)
        self.name = name
        self.position = position
        self.engine_type = engine_type
        self.cargo_type = cargo_type
        self.vehicle_type = vehicle_type
        self.owner = owner
        self.value = value
